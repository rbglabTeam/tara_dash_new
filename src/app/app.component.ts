import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from './services/user.service';




@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'taraDash';

  // Admin={}
  constructor(private route: ActivatedRoute,
    private router: Router,
    public user: UserService) {
      this.checkLocal()
  }
  checkLocal(){

    let currentUserDash=JSON.parse(localStorage.getItem('currentUserDash'))
    if(currentUserDash && currentUserDash.token){
     this.router.navigate(['/landing'])
     }else{
    this.router.navigate(['/login'])
     }

   }


}
