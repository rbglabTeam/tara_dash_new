import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxPaginationModule } from 'ngx-pagination';


import {LandingComponent} from './landing.component';
import {LandingRoutingModule} from './landing-routing.module'
import { CommonDashComponent } from '../common-dash/common-dash.component';
import { DashStationComponent } from '../admin/dash-station/dash-station.component';
import { DashUserComponent } from '../station/dash-user/dash-user.component';
import { ViewComponent } from '../admin/view/view.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RoleAccessComponent } from '../role-access/role-access.component';
import { CreateComponent } from '../admin/create/create.component';
import { DeactiveStationComponent } from '../admin/deactive-station/deactive-station.component';
import { ViewUserComponent } from '../station/view-user/view-user.component';
import { AddUserComponent } from '../station/add-user/add-user.component';
import { DeactiveUserComponent } from '../station/deactive-user/deactive-user.component';
import { MonthlyReportComponent } from '../report/monthly-report/monthly-report.component';
import { AreaTimeReportComponent } from '../report/area-time-report/area-time-report.component';
import { PrintDataUserComponent } from '../station/print-data-user/print-data-user.component';
import { PrintDataStationComponent } from '../admin/print-data-station/print-data-station.component';
import { WeatherConditionsReportComponent } from '../report/weather-conditions-report/weather-conditions-report.component';
import { NgxSpinnerService, NgxSpinnerModule } from 'ngx-spinner';
import { NguiDatetimePickerModule } from '@ngui/datetime-picker';
import { ClassificationRoadReportComponent } from '../report/classification-road-report/classification-road-report.component';
import { RoadEnvironmentReportComponent } from '../report/road-environment-report/road-environment-report.component';
import { RoadFeaturesReportComponent } from '../report/road-features-report/road-features-report.component';
import { JunctionTypeReportComponent } from '../report/junction-type-report/junction-type-report.component';
import { TrafficControlReportComponent } from '../report/traffic-control-report/traffic-control-report.component';
import { PedestrianInfrastructureReportComponent } from '../report/pedestrian-infrastructure-report/pedestrian-infrastructure-report.component';
import { TypeOfVehicleReportComponent } from '../report/type-of-vehicle-report/type-of-vehicle-report.component';
import { AgeImpactingVehiclesReportComponent } from '../report/age-impacting-vehicles-report/age-impacting-vehicles-report.component';
import { LoadConditionReportComponent } from '../report/load-condition-report/load-condition-report.component';
import { TypeOfCollisionReportComponent } from '../report/type-of-collision-report/type-of-collision-report.component';
import { TrafficViolationsReportComponent } from '../report/traffic-violations-report/traffic-violations-report.component';
import { UseNonuseSafetyReportComponent } from '../report/use-nonuse-safety-report/use-nonuse-safety-report.component';
import { LicenseOfDriversReportComponent } from '../report/license-of-drivers-report/license-of-drivers-report.component';
import { TypeofRoadUsersReportComponent } from '../report/typeof-road-users-report/typeof-road-users-report.component';
import { VictimsAgeSexReportComponent } from '../report/victims-age-sex-report/victims-age-sex-report.component';

@NgModule({
  declarations: [
    LandingComponent,
    // CommonDashComponent,
    // DashStationComponent,
    // DashUserComponent,
    // ViewComponent,
    // RoleAccessComponent,
    // CreateComponent,
    // DeactiveStationComponent,
    // PrintDataStationComponent,
    // ViewUserComponent,
    // AddUserComponent,
    // DeactiveUserComponent,
    // PrintDataUserComponent,

    //report

  //   MonthlyReportComponent,
  //   AreaTimeReportComponent,
  //   WeatherConditionsReportComponent,
  //   ClassificationRoadReportComponent,
  //   RoadEnvironmentReportComponent,
  //   RoadFeaturesReportComponent,
  //   JunctionTypeReportComponent,
  //   TrafficControlReportComponent,
  //   PedestrianInfrastructureReportComponent,
  //   TypeOfVehicleReportComponent,
  //   AgeImpactingVehiclesReportComponent,
  //   LoadConditionReportComponent,
  //   TypeOfCollisionReportComponent,
  //   TrafficViolationsReportComponent,
  //   UseNonuseSafetyReportComponent,
  //   LicenseOfDriversReportComponent,
  //   TypeofRoadUsersReportComponent,
  //   VictimsAgeSexReportComponent
  // 
] ,
  imports: [
    CommonModule,
    LandingRoutingModule,
    FormsModule,
    NguiDatetimePickerModule,
    ReactiveFormsModule,
    NgxPaginationModule,  
    NgxSpinnerModule  
  ],
  schemas:[
    CUSTOM_ELEMENTS_SCHEMA
  ],

})
export class LandingModule { }
