import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LandingComponent} from './landing.component';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

const routes: Routes = [
    {path:'',component:LandingComponent,
        children:[
            {path:'',pathMatch:"full",redirectTo:"commondash"},
            {path:'commondash',loadChildren:'../common-dash/common-dash.module#CommonDashModule'},
            {path:'viewStation',loadChildren:'../admin/view/view.module#ViewModule'},
            {path:'chart',loadChildren:'../admin/chart/chart.module#ChartModule'},
            {path:'createZone',loadChildren:'../role-access/role-access.module#RoleAccessModule'},
            {path:'createStation',loadChildren:'../admin/create/create.module#CreateModule'},
            { path: 'deactStation',loadChildren:'../admin/deactive-station/deactive-station.module#DeactiveStationModule'},
            { path: 'StationviewUser',loadChildren:'../station/view-user/view-user.module#ViewUserModule'},
            { path: 'StationaddUser', loadChildren:'../station/add-user/add-user.module#AddUserModule' },
            { path: 'StationdeactiveUser', loadChildren:'../station/deactive-user/deactive-user.module#DeactiveUserModule' },
            { path: 'viewStation/:id', loadChildren:'../admin/print-data-station/print-data-station.module#PrintDataStationModule' },
            {path:'sendMail',loadChildren:'../send-mail/send-mail.module#SendMailModule'},
            { path: 'StationviewUser/:id', loadChildren:'../station/print-data-user/print-data-user.module#PrintDataUserModule' },


            // 18 reports

            { path: 'monthlyReport', loadChildren:'../report/monthly-report/monthly-report.module#MonthlyReportModule' },
            { path: 'areaTimeReport', loadChildren:'../report/area-time-report/area-time-report.module#AreaTimeReportModule' },
            { path: 'weatherConditionsReport', loadChildren:'../report/weather-conditions-report/weather-conditions-report.module#WeatherConditionsReportModule' },
            { path: 'classificationOfRoad', loadChildren:'../report/classification-road-report/classification-road-report.module#ClassificationRoadReportModule' },
            { path: 'roadEnvironment', loadChildren:'../report/road-environment-report/road-environment-report.module#RoadEnvironmentReportModule' },
            { path: 'roadFeatures', loadChildren:'../report/road-features-report/road-features-report.module#RoadFeaturesReportModule' },
            { path: "junctionType", loadChildren:'../report/junction-type-report/junction-type-report.module#JunctionTypeReportModule' },
            { path: "trafficControl", loadChildren:'../report/traffic-control-report/traffic-control-report.module#TrafficControlReportModule'},
            { path: 'pedestrianInfrastructure', loadChildren:'../report/pedestrian-infrastructure-report/pedestrian-infrastructure-report.module#PedestrianInfrastructureReportModule' },
            { path: "impactingVehicleObjects", loadChildren:'../report/type-of-vehicle-report/type-of-vehicle-report.module#TypeOfVehicleReportModule' },
            { path: "ageImpactingVehicles", loadChildren:'../report/age-impacting-vehicles-report/age-impacting-vehicles-report.module#AgeImpactingVehiclesReportModule' },
            { path: "loadInvolvedVehicle", loadChildren:'../report/load-condition-report/load-condition-report.module#LoadConditionReportModule' },
            { path: 'collision', loadChildren:'../report/type-of-collision-report/type-of-collision-report.module#TypeOfCollisionReportModule' },
            { path: 'trafficViolations', loadChildren:'../report/traffic-violations-report/traffic-violations-report.module#TrafficViolationsReportModule' },
            { path: "useNonuseSafetyDevice", loadChildren:'../report/use-nonuse-safety-report/use-nonuse-safety-report.module#UseNonuseSafetyReportModule' },
            { path: 'licenseDrivers', loadChildren:'../report/license-of-drivers-report/license-of-drivers-report.module#LicenseOfDriversReportModule' },
           { path: 'roadUsers', loadChildren:'../report/typeof-road-users-report/typeof-road-users-report.module#TypeofRoadUsersReportModule' },
           { path: 'typeVictims', loadChildren:'../report/victims-age-sex-report/victims-age-sex-report.module#VictimsAgeSexReportModule' }

        ]
},

]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule,CommonModule,ReactiveFormsModule],
    declarations: [],
    schemas:[CUSTOM_ELEMENTS_SCHEMA]
})
export class LandingRoutingModule { }