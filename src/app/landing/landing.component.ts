import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from '../services/user.service';
import { NgxSpinnerService } from 'ngx-spinner';


@Component({
    selector: 'app-landing',
    templateUrl: './landing.component.html',
    styleUrls: ['./landing.component.scss']
})
export class LandingComponent implements OnInit {

    $: any;
    public local

    constructor(private route: ActivatedRoute,
        private router: Router, public user: UserService,
        public spinner:NgxSpinnerService) { }

    ngOnInit() {
        window.scrollTo(0, 0);
        $(document).ready(function () {

            'use strict';


            // ------------------------------------------------------- //
            // Adding fade effect to dropdowns
            // ------------------------------------------------------ //
            $('.dropdown').on('show.bs.dropdown', function () {
                $(this).find('.dropdown-menu').first().stop(true, true).fadeIn();
            });
            $('.dropdown').on('hide.bs.dropdown', function () {
                $(this).find('.dropdown-menu').first().stop(true, true).fadeOut();
            });


            // ------------------------------------------------------- //
            // Sidebar Functionality
            // ------------------------------------------------------ //
            $('#toggle-btn').on('click', function (e) {

                e.preventDefault();

                if ($('#toggle-btn').hasClass('active')) {
                    $(this).removeClass('active');

                    $('.side-navbar').removeClass('shrinked');
                    $('.content-inner').removeClass('active');
                } else {
                    $(this).addClass('active');

                    $('.side-navbar').addClass('shrinked');
                    $('.content-inner').addClass('active');

                }
                $(document).trigger('sidebarChanged');

                if ($(window).outerWidth() > 1183) {
                    if ($('#toggle-btn').hasClass('active')) {
                        $('.navbar-header .brand-small').hide();
                        $('.navbar-header .brand-big').show();
                    } else {
                        $('.navbar-header .brand-small').show();
                        $('.navbar-header .brand-big').hide();
                    }
                }

                if ($(window).outerWidth() < 1183) {
                    $('.navbar-header .brand-small').show();
                }
            });

            // ------------------------------------------------------- //
            // Universal Form Validation
            // ------------------------------------------------------ //

            $('.form-validate').each(function () {
                $(this).validate({
                    errorElement: "div",
                    errorClass: 'is-invalid',
                    validClass: 'is-valid',
                    ignore: ':hidden:not(.summernote, .checkbox-template, .form-control-custom),.note-editable.card-block',
                    errorPlacement: function (error, element) {
                        // Add the `invalid-feedback` class to the error element
                        error.addClass("invalid-feedback");
                        console.log(element);
                        if (element.prop("type") === "checkbox") {
                            error.insertAfter(element.siblings("label"));
                        }
                        else {
                            error.insertAfter(element);
                        }
                    }
                });

            });

            // ------------------------------------------------------- //
            // Material Inputs
            // ------------------------------------------------------ //

            var materialInputs = $('input.input-material');

            // activate labels for prefilled values
            materialInputs.filter(function () { return $(this).val() !== ""; }).siblings('.label-material').addClass('active');

            // move label on focus
            materialInputs.on('focus', function () {
                $(this).siblings('.label-material').addClass('active');
            });

            // remove/keep label on blur
            materialInputs.on('blur', function () {
                $(this).siblings('.label-material').removeClass('active');

                if ($(this).val() !== '') {
                    $(this).siblings('.label-material').addClass('active');
                } else {
                    $(this).siblings('.label-material').removeClass('active');
                }
            });

            // ------------------------------------------------------- //
            // Footer 
            // ------------------------------------------------------ //   

            var contentInner = $('.content-inner');

            $(document).on('sidebarChanged', function () {
                adjustFooter();
            });

            $(window).on('resize', function () {
                adjustFooter();
            })

            function adjustFooter() {
                var footerBlockHeight = $('.main-footer').outerHeight();
                contentInner.css('padding-bottom', footerBlockHeight + 'px');
            }

            // ------------------------------------------------------- //
            // External links to new window
            // ------------------------------------------------------ //
            $('.external').on('click', function (e) {

                e.preventDefault();
                window.open($(this).attr("href"));
            });



        });
        this.getLocalstorage()

    }
    getLocalstorage(): any {

        this.local = JSON.parse(localStorage.getItem('currentUserDash'))

    }
    removeLocal() {
        this.router.navigate(['login'])
        localStorage.removeItem('currentUserDash')


    }

    admin=[
        {
            link:"/landing/commondash",
            name:"Dashboard",
            icon:"icon-home"
        },
        {
            link:"/landing/viewStation",
            name:"View Data",
            icon:"icon-grid"
        },
        {
            link:"/landing/chart",
            name:"Chart",
            icon:"fa fa-bar-chart"
        },
        {
            link:"/landing/createZone",
            name:"Create Zone",
            icon:"fa fa-plus-circle"
        },
        {
            link:"/landing/createStation",
            name:"Create Station",
            icon:"fa fa-plus-circle"
        },
        {
            link:"/landing/deactStation",
            name:"De-activate Station",
            icon:"icon-padnote"
        },
        
    ]

    station=[
        {
            link:"/landing/commondash",
            name:"Dashboard",
            icon:"icon-home"
        },
        {
            link:"/landing/StationviewUser",
            name:"View Data",
            icon:"icon-grid"
        },
        {
            link:"/landing/StationaddUser",
            name:"Create User",
            icon:"fa fa-bar-chart"
        },
        {
            link:"/landing/StationdeactiveUser",
            name:"De-activate User",
            icon:"icon-padnote"
        }
    ]
   
    reports=[
        {
            link:"/landing/monthlyReport",
            name:"Month of the Year"  
        },
        {
            link:"/landing/areaTimeReport",
            name:"Area and Time of the Day"  
        },
        {
            link:"/landing/weatherConditionsReport",
            name:"Weather Conditions"  
        },
        {
            link:"/landing/classificationOfRoad",
            name:"Classification of Road"  
        },
        {
            link:"/landing/roadEnvironment",
            name:"Road Environment"  
        },
        {
            link:"/landing/roadFeatures",
            name:"Road Features"  
        },
        {
            link:"/landing/junctionType",
            name:"Junction Type"  
        },
        {
            link:"/landing/trafficControl",
            name:" Traffic Control at Junction"  
        },
        {
            link:"/landing/pedestrianInfrastructure",
            name:"Pedestrian Infrastructure"  
        },
        {
            link:"/landing/impactingVehicleObjects",
            name:"Type of Impacting Vehicle/Objects"  
        },
        {
            link:"/landing/ageImpactingVehicles",
            name:"Age of Impacting Vehicles"  
        },
        {
            link:"/landing/loadInvolvedVehicle",
            name:"Load Condition of Involved Vehicles"  
        },
        {
            link:"/landing/collision",
            name:"Type of Collision"  
        },
        {
            link:"/landing/trafficViolations",
            name:"Type of Traffic Violations"  
        },
        {
            link:"/landing/useNonuseSafetyDevice",
            name:"Use/Non-use of Safety Device by Victim"  
        },
        {
            link:"/landing/licenseDrivers",
            name:"License of Drivers"  
        },
        {
            link:"/landing/roadUsers",
            name:"Type of Road Users"  
        },
        {
            link:"/landing/typeVictims",
            name:"Type of Victims"  
        },
    ]

}
