import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';





const routes: Routes = [
  {path:'login',loadChildren:'../app/login/login.module#LoginModule'},
  {path:'',loadChildren:'../app/login/login.module#LoginModule'},
  {path:'landing',loadChildren:'../app/landing/landing.module#LandingModule'},
  ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule,ReactiveFormsModule, CommonModule ]
})
export class AppRoutingModule { }
