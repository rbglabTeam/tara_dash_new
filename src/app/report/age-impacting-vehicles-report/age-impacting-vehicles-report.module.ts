import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AgeImpactingVehiclesReportComponent } from './age-impacting-vehicles-report.component';
import { AgeImpactVechileRoutingModule } from './age-impacting-vechiles-routing.module';
import { FormsModule } from '@angular/forms';
import { NguiDatetimePickerModule } from '@ngui/datetime-picker';
import { NgxSpinnerModule } from 'ngx-spinner';

@NgModule({
  declarations: [AgeImpactingVehiclesReportComponent],
  imports: [
    CommonModule,
    AgeImpactVechileRoutingModule,
    FormsModule,
    NguiDatetimePickerModule,
    NgxSpinnerModule
  ],
  schemas:[
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class AgeImpactingVehiclesReportModule { }
