import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AgeImpactingVehiclesReportComponent } from './age-impacting-vehicles-report.component';

const routes: Routes = [
    {path:'',component:AgeImpactingVehiclesReportComponent}
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    declarations: [],
})
export class AgeImpactVechileRoutingModule { }