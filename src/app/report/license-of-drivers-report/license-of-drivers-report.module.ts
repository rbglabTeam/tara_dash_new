import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LicenseDriverRoutingModule } from './license-of-drivers-report-routing.module';
import { LicenseOfDriversReportComponent } from './license-of-drivers-report.component';
import { FormsModule } from '@angular/forms';
import { NguiDatetimePickerModule } from '@ngui/datetime-picker';
import { NgxSpinnerModule } from 'ngx-spinner';

@NgModule({
  declarations: [LicenseOfDriversReportComponent],
  imports: [
    CommonModule,
    LicenseDriverRoutingModule,
    FormsModule,
    NguiDatetimePickerModule,
    NgxSpinnerModule
  ],
  schemas:[
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class LicenseOfDriversReportModule { }
