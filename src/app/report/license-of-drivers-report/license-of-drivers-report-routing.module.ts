import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LicenseOfDriversReportComponent } from './license-of-drivers-report.component';

const routes: Routes = [
    {path:'',component:LicenseOfDriversReportComponent}
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    declarations: [],
})
export class LicenseDriverRoutingModule { }