import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PedestrianInfrastructureReportComponent } from './pedestrian-infrastructure-report.component';
import { PedestrainInfrastructureRoutingModule } from './pedestrian-infrastructure-report-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NguiDatetimePickerModule } from '@ngui/datetime-picker';
import { NgxSpinnerModule } from 'ngx-spinner';

@NgModule({
  declarations: [PedestrianInfrastructureReportComponent],
  imports: [
    CommonModule,
    PedestrainInfrastructureRoutingModule,
    FormsModule,ReactiveFormsModule,
    NguiDatetimePickerModule,
    NgxSpinnerModule
  ],
  schemas:[
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class PedestrianInfrastructureReportModule { }
