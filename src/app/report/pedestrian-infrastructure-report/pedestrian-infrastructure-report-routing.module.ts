import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PedestrianInfrastructureReportComponent } from './pedestrian-infrastructure-report.component';
import { FormsModule } from '@angular/forms';

const routes: Routes = [
    {path:'',component:PedestrianInfrastructureReportComponent}
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule,FormsModule],
    declarations: [],
})
export class PedestrainInfrastructureRoutingModule { }