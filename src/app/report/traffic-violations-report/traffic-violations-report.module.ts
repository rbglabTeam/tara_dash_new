import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TrafficViolationRoutingModule } from './traffic-violations-report-routing.module';
import { TrafficViolationsReportComponent } from './traffic-violations-report.component';
import { FormsModule } from '@angular/forms';
import { NguiDatetimePickerModule } from '@ngui/datetime-picker';
import { NgxSpinnerModule } from 'ngx-spinner';

@NgModule({
  declarations: [TrafficViolationsReportComponent],
  imports: [
    CommonModule,
    TrafficViolationRoutingModule,
    FormsModule,
    NguiDatetimePickerModule,
    NgxSpinnerModule
  ]
})
export class TrafficViolationsReportModule { }
