import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TrafficViolationsReportComponent } from './traffic-violations-report.component';

const routes: Routes = [
    {path:'',component:TrafficViolationsReportComponent}
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    declarations: [],
})
export class TrafficViolationRoutingModule { }