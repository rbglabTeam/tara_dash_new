import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TypeofRoadUsersReportComponent } from './typeof-road-users-report.component';

const routes: Routes = [
    {path:'',component:TypeofRoadUsersReportComponent}
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    declarations: [],
})
export class TypeRoadUserRoutingModule { }