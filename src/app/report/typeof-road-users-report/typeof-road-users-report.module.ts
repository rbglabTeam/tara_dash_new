import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TypeRoadUserRoutingModule } from './typeof-road-users-report-routing.module';
import { TypeofRoadUsersReportComponent } from './typeof-road-users-report.component';
import { FormsModule } from '@angular/forms';
import { NguiDatetimePickerModule } from '@ngui/datetime-picker';
import { NgxSpinnerModule } from 'ngx-spinner';

@NgModule({
  declarations: [TypeofRoadUsersReportComponent],
  imports: [
    CommonModule,
    TypeRoadUserRoutingModule,
    FormsModule,
    NguiDatetimePickerModule,
    NgxSpinnerModule
  ],
  schemas:[
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class TypeofRoadUsersReportModule { }
