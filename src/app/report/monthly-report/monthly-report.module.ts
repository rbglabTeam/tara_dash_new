import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MonthlyReportComponent } from './monthly-report.component';
import { MonthyReportRoutingModule } from './monthly-report-routing.module';
import { NgxSpinnerModule } from 'ngx-spinner';
import { NguiDatetimePickerModule } from '@ngui/datetime-picker';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [MonthlyReportComponent],
  imports: [
    CommonModule,MonthyReportRoutingModule,NgxSpinnerModule,NguiDatetimePickerModule,FormsModule
  ]
})
export class MonthlyReportModule { }
