import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import swal from 'sweetalert';
import { NgxSpinnerService } from 'ngx-spinner';
import { UserService } from 'src/app/services/user.service';
declare var $: any;


@Component({
  selector: 'app-monthly-report',
  templateUrl: './monthly-report.component.html',
  styleUrls: ['./monthly-report.component.scss']
})
export class MonthlyReportComponent implements OnInit {
  model: any = {};
  form: FormGroup
  error: any = { isError: false, errorMessage: '' };
  isValidDate: any;
  public entries = [];
  allStations: any;
  stationId: any;
  local: any;
  constructor(public api: ApiService,
             private fb: FormBuilder, 
             private spinner: NgxSpinnerService,
             public user:UserService
    ) {
      this.user.setAccidents([])
    // this.createForm();
    this.model = {
      StartDate: "",
      EndDate: ""
    }
  }

  ngOnInit() {
    window.scrollTo(0, 0);
    // this.getCsv()
    this.getStations();
    this.getLocalstorage();
  }

  getStations() {
    this.api.do('get', 'admin/stations/list', {}, true).then(res => {
      this.allStations = res['data'];
      console.log(res);
    }, err => {
      console.log(err);
    });
  }

  getCsv(){
    const data= [
      {id: 1, name: 'a'},
      {id: 2, name: 'b'},
      {id: 3, name: 'c'}
   ]
    const replacer = (key, value) => value === null ? '' : value; // specify how you want to handle null values here
    const header = Object.keys(data[0]);
    let csv = data.map(row => header.map(fieldName => JSON.stringify(row[fieldName], replacer)).join(','));
    csv.unshift(header.join(','));
    let csvArray = csv.join('\r\n');

    var a = document.createElement('a');
    var blob = new Blob([csvArray], {type: 'text/csv' }),
    url = window.URL.createObjectURL(blob);

    a.href = url;
    a.download = "myFile.csv";
    a.click();
    window.URL.revokeObjectURL(url);
    a.remove();
  }
  getLocalstorage(): any {
    this.local = JSON.parse(localStorage.getItem('currentUserDash'))
    console.log(this.local)
}

  onSubmit() {
    this.entries = [];
    const packet = {
      station_id: this.stationId,
      from_date: this.model.StartDate,
      to_date: this.model.EndDate
    };
    let currentDate = new Date();
    this.isValidDate = this.validateDates(this.model.StartDate, this.model.EndDate, currentDate);
    if (this.isValidDate == true) {
      this.spinner.show()
      this.api.do('get', 'dash/station/accident/list/report', packet, true).then(res => {
        
        this.spinner.hide();
        swal({
          title: res['status'],
          icon: res['icon'],
          timer: 1000
        });
        this.entries = res['data'];
        $(document).ready(function() {
          $('#example').DataTable( {
          } );
          })
        console.log(this.entries);
      });
    }
  }
  validateDates(sDate: string, eDate: string, currentDate: Object) {
    // this.isValidDate = true;
    if ((sDate == "" || eDate == "")) {
      this.error = {
        isError: true, errorMessage: swal({
          text: "Start date and end date are required",
          icon: "warning",
          dangerMode: true,
        })
      };
      this.isValidDate = false;
    }

    else if ((eDate) < (sDate)) {
      this.error = {
        isError: true, errorMessage: swal({
          text: "To date should be grater then From date.",
          icon: "warning",
          dangerMode: true,
        })
      };
      this.isValidDate = false;
    }
    else if ((currentDate) < (sDate) || (currentDate) < (eDate)) {
      this.error = {
        isError: true, errorMessage: swal({
          text: "Future date should not be selected",
          icon: "warning",
          dangerMode: true,
        })
      };
      this.isValidDate = false;
    }
    else {
      this.isValidDate = true;
    }
    return this.isValidDate;
  }

   printOne() {

    var divToPrint = document.getElementById('tableOne');


    var newWin =window.open('', '_blank','scrollbars=yes,menubar=no,toolbar=no,location=no,status=no,titlebar=no,;')

    
    newWin.document.open();

    newWin.document.write('<html><head><style>@page{size: landscape;}</style></head><body onload="window.print()">' + divToPrint.innerHTML+ '</body></html>');

    newWin.document.close();

    setTimeout(function () { newWin.close(); }, 10);
    
  }

  printTwo() {

    var divToPrint = document.getElementById('tableTwo');


    var newWin =window.open('', '_blank','scrollbars=yes,menubar=no,toolbar=no,location=no,status=no,titlebar=no,;')

    
    newWin.document.open();

    newWin.document.write('<html><head><style>@page{size: landscape;}</style></head><body onload="window.print()">' + divToPrint.innerHTML+ '</body></html>');

    newWin.document.close();

    setTimeout(function () { newWin.close(); }, 10);
    
  }
  // printPDF()
  //   {
  //     var html = document.querySelector("table").outerHTML;
  //   this.export_table_to_csv(html, "table.csv");
  // }

  
  export_table_to_csv(html,filename){
    var csv = [];
  var rows = document.querySelectorAll("table tr");
  
  var css = document.querySelector  ('colspan')
	
    for (var i = 0; i < rows.length; i++) {
    var row = [],
     cols = rows[i].querySelectorAll("td, th") ;
		
        for (var j = 0; j < cols.length; j++) 
            row.push(cols[j]['innerText']);
        
		csv.push(row.join(","));		
	}

    // Download CSV
    this.download_csv(csv.join("\n"), filename);
  }

  download_csv(csv, filename) {
    var csvFile;
    var downloadLink;

    // CSV FILE
    csvFile = new Blob([csv], {type: "text/csv"});

    // Download link
    downloadLink = document.createElement("a");

    // File name
    downloadLink.download = filename;

    // We have to create a link to the file
    downloadLink.href = window.URL.createObjectURL(csvFile);

    // Make sure that the link is not displayed
    downloadLink.style.display = "none";

    // Add the link to your DOM
    document.body.appendChild(downloadLink);

    // Lanzamos
    downloadLink.click();
}
  

  
}

