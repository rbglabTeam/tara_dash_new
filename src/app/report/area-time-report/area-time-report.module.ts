import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AreaTimeReportComponent } from './area-time-report.component';
import { AreaTimeReportRoutingModule } from './area-time-report-routing.module';
import { NgxSpinnerModule } from 'ngx-spinner';
import { NguiDatetimePickerModule } from '@ngui/datetime-picker';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [AreaTimeReportComponent],
  imports: [
    CommonModule,
    FormsModule,
    AreaTimeReportRoutingModule,
    NgxSpinnerModule,
    NguiDatetimePickerModule
  ]
})
export class AreaTimeReportModule { }
