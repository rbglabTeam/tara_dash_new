import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { AreaTimeReportComponent } from './area-time-report.component';

const routes: Routes = [
    {path:'',component:AreaTimeReportComponent}
]

@NgModule({
    imports: [RouterModule.forChild(routes),FormsModule,FormsModule],
    exports: [RouterModule,CommonModule],
    declarations: [],
})
export class AreaTimeReportRoutingModule { }