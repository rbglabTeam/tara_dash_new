import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { element } from '@angular/core/src/render3';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import swal from 'sweetalert';
import { NgxSpinnerService } from 'ngx-spinner';
import { UserService } from 'src/app/services/user.service';
@Component({
  selector: 'app-area-time-report',
  templateUrl: './area-time-report.component.html',
  styleUrls: ['./area-time-report.component.scss']
})
export class AreaTimeReportComponent implements OnInit {
  model: any = {};
  form: FormGroup;
  error: any = { isError: false, errorMessage: '' };
  isValidDate: any;
  public entries = [];
  public ruralType={
     sixToNine : [],
     nineToTwelve : [],
     twelveToFifteen : [],
     fifteenToEighteen : [],
     eighteenToTwentyOne : [],
     twentyOneToTwentyFour : [],
     twentyFourToThree : [],
     threeToSix : [],
     sixToNineSum: {},
     nineToTwelveSum: {},
     twelveToFifteenSum: {},
     fifteenToEighteenSum: {},
     eighteenToTwentyOneSum: {},
     twentyOneToTwentyFourSum: {},
     twentyFourToThreeSum: {},
     threeToSixSum: {}
  }
  public urbanType={
    sixToNine : [],
    nineToTwelve : [],
    twelveToFifteen : [],
    fifteenToEighteen : [],
    eighteenToTwentyOne : [],
    twentyOneToTwentyFour : [],
    twentyFourToThree : [],
    threeToSix : [],
    sixToNineSum: {},
    nineToTwelveSum: {},
    twelveToFifteenSum: {},
    fifteenToEighteenSum: {},
    eighteenToTwentyOneSum: {},
    twentyOneToTwentyFourSum: {},
    twentyFourToThreeSum: {},
    threeToSixSum: {}
 }
  public sixToNine = [];
  public nineToTwelve = [];
  public twelveToFifteen = [];
  public fifteenToEighteen = [];
  public eighteenToTwentyOne = [];
  public twentyOneToTwentyFour = [];
  public twentyFourToThree = [];
  public threeToSix = []
  public sixToNineSum: {};
  public nineToTwelveSum: {};
  public twelveToFifteenSum: {};
  public fifteenToEighteenSum: {};
  public eighteenToTwentyOneSum: {};
  public twentyOneToTwentyFourSum: {};
  public twentyFourToThreeSum: {};
  public threeToSixSum: {};
  allStations: any;
  stationId: any;
  local: any;
  constructor(public api: ApiService,
             private fb: FormBuilder,
            public spinner:NgxSpinnerService,
            public user:UserService
    ) {
      this.user.setAccidents([])
    this.model = {
      StartDate: "",
      EndDate: ""
    }
  }

  ngOnInit() {
    window.scrollTo(0, 0);
    this.getStations();
    this.getLocalstorage();
  }

  getStations() {
    this.api.do('get', 'admin/stations/list', {}, true).then(res => {
      this.allStations = res['data'];
      console.log(res);
    }, err => {
      console.log(err);
    });
  }
  onSubmit() {

    this.ruralType.sixToNine = [];
    this.ruralType.nineToTwelve = [];
    this.ruralType.twelveToFifteen = [];
    this.ruralType.fifteenToEighteen = [];
    this.ruralType.eighteenToTwentyOne = [];
    this.ruralType.twentyOneToTwentyFour = [];
    this.ruralType.twentyFourToThree = [];
    this.ruralType.threeToSix = [];
    this.urbanType.sixToNine = [];
    this.urbanType.nineToTwelve = [];
    this.urbanType.twelveToFifteen = [];
    this.urbanType.fifteenToEighteen = [];
    this.urbanType.eighteenToTwentyOne = [];
    this.urbanType.twentyOneToTwentyFour = [];
    this.urbanType.twentyFourToThree = [];
    this.urbanType.threeToSix = []

    this.entries = [];
    const packet = {
      station_id: this.stationId,
      from_date: this.model.StartDate,
      to_date: this.model.EndDate
    };
   let currentDate = new Date();
    this.isValidDate = this.validateDates(this.model.StartDate, this.model.EndDate, currentDate);
    if (this.isValidDate == true) {
      this.spinner.show()
      this.api.do('get', 'dash/station/accident/list/persons/report', packet, true).then(res => {
        this.spinner.hide()
        swal({
          title: res['status'],
          icon: res['icon'],
          timer: 1000
        });
        this.entries = res['data']
        console.log(this.entries);
        this.urbanType.sixToNine.push(this.entries['urbanObject']['six'], this.entries['urbanObject']['seven'], this.entries['urbanObject']['eight'])
        this.urbanType.nineToTwelve.push(this.entries['urbanObject']['nine'], this.entries['urbanObject']['ten'], this.entries['urbanObject']['eleven'])
        this.urbanType.twelveToFifteen.push(this.entries['urbanObject']['twelve'], this.entries['urbanObject']['thirteen'], this.entries['urbanObject']['fourteen'])
        this.urbanType.fifteenToEighteen.push(this.entries['urbanObject']['fifteen'], this.entries['urbanObject']['sixteen'], this.entries['urbanObject']['seventeen'])
        this.urbanType.eighteenToTwentyOne.push(this.entries['urbanObject']['eighteen'], this.entries['urbanObject']['nineteen'], this.entries['urbanObject']['twenty'])
        this.urbanType.twentyOneToTwentyFour.push(this.entries['urbanObject']['twentyone'], this.entries['urbanObject']['twentytwo'], this.entries['urbanObject']['twentythree'])
        this.urbanType.twentyFourToThree.push(this.entries['urbanObject']['twentyfour'], this.entries['urbanObject']['one'], this.entries['urbanObject']['two'])
        this.urbanType.threeToSix.push(this.entries['urbanObject']['three'], this.entries['urbanObject']['four'], this.entries['urbanObject']['five'])
        // console.log(this.ruralType.sixToNine, this.ruralType.nineToTwelve, this.ruralType.twelveToFifteen, this.ruralType.fifteenToEighteen, this.ruralType.eighteenToTwentyOne, this.ruralType.twentyOneToTwentyFour, this.ruralType.twentyFourToThree, this.ruralType.threeToSix);

        this.ruralType.sixToNine.push(this.entries['ruralObject']['six'], this.entries['ruralObject']['seven'], this.entries['ruralObject']['eight'])
        this.ruralType.nineToTwelve.push(this.entries['ruralObject']['nine'], this.entries['ruralObject']['ten'], this.entries['ruralObject']['eleven'])
        this.ruralType.twelveToFifteen.push(this.entries['ruralObject']['twelve'], this.entries['ruralObject']['thirteen'], this.entries['ruralObject']['fourteen'])
        this.ruralType.fifteenToEighteen.push(this.entries['ruralObject']['fifteen'], this.entries['ruralObject']['sixteen'], this.entries['ruralObject']['seventeen'])
        this.ruralType.eighteenToTwentyOne.push(this.entries['ruralObject']['eighteen'], this.entries['ruralObject']['nineteen'], this.entries['ruralObject']['twenty'])
        this.ruralType.twentyOneToTwentyFour.push(this.entries['ruralObject']['twentyone'], this.entries['ruralObject']['twentytwo'], this.entries['ruralObject']['twentythree'])
        this.ruralType.twentyFourToThree.push(this.entries['ruralObject']['twentyfour'], this.entries['ruralObject']['one'], this.entries['ruralObject']['two'])
        this.ruralType.threeToSix.push(this.entries['ruralObject']['three'], this.entries['ruralObject']['four'], this.entries['ruralObject']['five'])
        // console.log(this.urbanType.sixToNine, this.urbanType.nineToTwelve, this.urbanType.twelveToFifteen, this.urbanType.fifteenToEighteen, this.urbanType.eighteenToTwentyOne, this.urbanType.twentyOneToTwentyFour, this.urbanType.twentyFourToThree, this.urbanType.threeToSix);


        this.urbanType.sixToNineSum = this.calculateSum(this.urbanType.sixToNine);
        this.urbanType.nineToTwelveSum = this.calculateSum(this.urbanType.nineToTwelve)
        this.urbanType.twelveToFifteenSum = this.calculateSum(this.urbanType.twelveToFifteen)
        this.urbanType.fifteenToEighteenSum = this.calculateSum(this.urbanType.fifteenToEighteen)
        this.urbanType.eighteenToTwentyOneSum = this.calculateSum(this.urbanType.eighteenToTwentyOne)
        this.urbanType.twentyOneToTwentyFourSum = this.calculateSum(this.urbanType.twentyOneToTwentyFour)
        this.urbanType.twentyFourToThreeSum = this.calculateSum(this.urbanType.twentyFourToThree)
        this.urbanType.threeToSixSum = this.calculateSum(this.urbanType.threeToSix)
        // console.log(this.urbanType.sixToNineSum, this.urbanType.nineToTwelveSum, this.urbanType.twelveToFifteenSum, this.urbanType.fifteenToEighteenSum, this.urbanType.eighteenToTwentyOneSum, this.urbanType.twentyOneToTwentyFourSum, this.urbanType.twentyFourToThreeSum, this.urbanType.threeToSixSum);

        this.ruralType.sixToNineSum = this.calculateSum(this.ruralType.sixToNine);
        this.ruralType.nineToTwelveSum = this.calculateSum(this.ruralType.nineToTwelve)
        this.ruralType.twelveToFifteenSum = this.calculateSum(this.ruralType.twelveToFifteen)
        this.ruralType.fifteenToEighteenSum = this.calculateSum(this.ruralType.fifteenToEighteen)
        this.ruralType.eighteenToTwentyOneSum = this.calculateSum(this.ruralType.eighteenToTwentyOne)
        this.ruralType.twentyOneToTwentyFourSum = this.calculateSum(this.ruralType.twentyOneToTwentyFour)
        this.ruralType.twentyFourToThreeSum = this.calculateSum(this.ruralType.twentyFourToThree)
        this.ruralType.threeToSixSum = this.calculateSum(this.ruralType.threeToSix)
        // console.log(this.ruralType.sixToNineSum, this.ruralType.nineToTwelveSum, this.ruralType.twelveToFifteenSum, this.ruralType.fifteenToEighteenSum, this.ruralType.eighteenToTwentyOneSum, this.ruralType.twentyOneToTwentyFourSum, this.ruralType.twentyFourToThreeSum, this.ruralType.threeToSixSum);

        

      })
    }
  }




  validateDates(sDate: string, eDate: string, currentDate: Object) {
    // this.isValidDate = true;
    if ((sDate == "" || eDate == "")) {
      this.error = {
        isError: true, errorMessage: swal({
          text: "Start date and end date are required",
          icon: "warning",
          dangerMode: true,
        })
      };
      this.isValidDate = false;
    }

    else if ((eDate) < (sDate)) {
      this.error = {
        isError: true, errorMessage: swal({
          text: "To date should be grater then From date.",
          icon: "warning",
          dangerMode: true,
        })
      };
      this.isValidDate = false;
    }
    else if ((currentDate) < (sDate) || (currentDate) < (eDate)) {
      this.error = {
        isError: true, errorMessage: swal({
          text: "Future date should not be selected",
          icon: "warning",
          dangerMode: true,
        })
      };
      this.isValidDate = false;
    }
    else {
      this.isValidDate = true;
    }
    return this.isValidDate;
  }


  calculateSum(data) {
    let result = {}
    for (var i = 0; i < data.length; i++) {
      var keys = Object.keys(data[i])
      for (var j = 0; j < keys.length; j++) {
        if (result[keys[j]]) {
          result[keys[j]] = result[keys[j]] + data[i][keys[j]]
        } else {
          result[keys[j]] = data[i][keys[j]]
        }
      }
    }
    return result;
  }

  getLocalstorage(): any {
    this.local = JSON.parse(localStorage.getItem('currentUserDash'))
    console.log(this.local)
}

printOne() {

  var divToPrint = document.getElementById('tableOne');


  var newWin =window.open('', '_blank','scrollbars=yes,menubar=no,toolbar=no,location=no,status=no,titlebar=no,;')

  
  newWin.document.open();

  newWin.document.write('<html><head><style>@page{size:a3 portrait;}</style></head><body onload="window.print()">' + divToPrint.innerHTML+ '</body></html>');

  newWin.document.close();

  setTimeout(function () { newWin.close(); }, 10);
  
}

printTwo() {

  var divToPrint = document.getElementById('tableTwo');


  var newWin =window.open('', '_blank','scrollbars=yes,menubar=no,toolbar=no,location=no,status=no,titlebar=no,;')

  
  newWin.document.open();

  newWin.document.write('<html><head><style>@page{size:a3 portrait;}</style></head><body onload="window.print()">' + divToPrint.innerHTML+ '</body></html>');

  newWin.document.close();

  setTimeout(function () { newWin.close(); }, 10);
  
}

}






