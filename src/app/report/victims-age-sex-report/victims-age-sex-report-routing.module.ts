import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VictimsAgeSexReportComponent } from './victims-age-sex-report.component';

const routes: Routes = [
    {path:'',component:VictimsAgeSexReportComponent}
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    declarations: [],
})
export class VictimsAgeSexRoutingModule { }