import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VictimsAgeSexRoutingModule } from './victims-age-sex-report-routing.module';
import { VictimsAgeSexReportComponent } from './victims-age-sex-report.component';
import { FormsModule } from '@angular/forms';
import { NguiDatetimePickerModule } from '@ngui/datetime-picker';
import { NgxSpinnerModule } from 'ngx-spinner';

@NgModule({
  declarations: [VictimsAgeSexReportComponent],
  imports: [
    CommonModule,
    VictimsAgeSexRoutingModule,
    FormsModule,
    NguiDatetimePickerModule,
    NgxSpinnerModule
  ],
  schemas:[
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class VictimsAgeSexReportModule { }
