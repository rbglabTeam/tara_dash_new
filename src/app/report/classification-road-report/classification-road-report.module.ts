import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClassificationRoadReportComponent } from './classification-road-report.component';
import { ClassificationRoadRoutingModule } from './classification-road-report-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NguiDatetimePickerModule } from '@ngui/datetime-picker';
import { NgxSpinnerModule } from 'ngx-spinner';

@NgModule({
  declarations: [ClassificationRoadReportComponent],
  imports: [
    CommonModule,ClassificationRoadRoutingModule,FormsModule,NguiDatetimePickerModule,NgxSpinnerModule,ReactiveFormsModule
  ]
})
export class ClassificationRoadReportModule { }
