import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClassificationRoadReportComponent } from './classification-road-report.component';
import { FormsModule } from '@angular/forms';

const routes: Routes = [
    {path:'',component:ClassificationRoadReportComponent}
]

@NgModule({
    imports: [RouterModule.forChild(routes),FormsModule],
    exports: [RouterModule],
    declarations: [],
})
export class ClassificationRoadRoutingModule { }