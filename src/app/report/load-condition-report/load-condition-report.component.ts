import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import swal from 'sweetalert';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-load-condition-report',
  templateUrl: './load-condition-report.component.html',
  styleUrls: ['./load-condition-report.component.scss']
})
export class LoadConditionReportComponent implements OnInit {

  model: any = {};
  form: FormGroup;
  error: any = { isError: false, errorMessage: '' };
  isValidDate: any;
  public entries = [];
  constructor(public api: ApiService,
              private fb: FormBuilder,
              public user:UserService
    ) {
      this.user.setAccidents([])
    // this.createForm();
    this.model = {
      StartDate: "",
      EndDate: ""
    }
  }

  ngOnInit() {
    window.scrollTo(0, 0);
  }

  onSubmit() {
    this.entries = [];
    const packet = {
      from_date: this.model.StartDate,
      to_date: this.model.EndDate
    };
    let currentDate = new Date();
    this.isValidDate = this.validateDates(this.model.StartDate, this.model.EndDate, currentDate);
    if (this.isValidDate == true) {
      this.api.do('get', '', packet, true).then(res => {
        this.entries = res['data'];
        console.log(this.entries);
      });
    }
  }
  validateDates(sDate: string, eDate: string, currentDate: Object) {
    // this.isValidDate = true;
    if ((sDate == "" || eDate == "")) {
      this.error = {
        isError: true, errorMessage: swal({
          text: "Start date and end date are required",
          icon: "warning",
          dangerMode: true,
        })
      };
      this.isValidDate = false;
    }

    else if ((eDate) < (sDate)) {
      this.error = {
        isError: true, errorMessage: swal({
          text: "To date should be grater then From date.",
          icon: "warning",
          dangerMode: true,
        })
      };
      this.isValidDate = false;
    }
    else if ((currentDate) < (sDate) || (currentDate) < (eDate)) {
      this.error = {
        isError: true, errorMessage: swal({
          text: "Future date should not be selected",
          icon: "warning",
          dangerMode: true,
        })
      };
      this.isValidDate = false;
    }
    else {
      this.isValidDate = true;
    }
    return this.isValidDate;
  }



}
