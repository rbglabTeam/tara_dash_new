import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoadConditionReportComponent } from './load-condition-report.component';

const routes: Routes = [
    {path:'',component:LoadConditionReportComponent}
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    declarations: [],
})
export class LoadConditionRoutingModule { }