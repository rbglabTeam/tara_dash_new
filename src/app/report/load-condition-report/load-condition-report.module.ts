import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoadConditionRoutingModule } from './load-condition-report-routing.module';
import { LoadConditionReportComponent } from './load-condition-report.component';
import { FormsModule } from '@angular/forms';
import { NguiDatetimePickerModule } from '@ngui/datetime-picker';
import { NgxSpinnerModule } from 'ngx-spinner';

@NgModule({
  declarations: [LoadConditionReportComponent],
  imports: [
    CommonModule,
    LoadConditionRoutingModule,
    FormsModule,
    NguiDatetimePickerModule,
    NgxSpinnerModule
  ],
  schemas:[
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class LoadConditionReportModule { }
