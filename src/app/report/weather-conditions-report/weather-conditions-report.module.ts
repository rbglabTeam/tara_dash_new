import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxSpinnerModule } from 'ngx-spinner';
import { NguiDatetimePickerModule } from '@ngui/datetime-picker';
import { WeatherConditionsReportComponent } from './weather-conditions-report.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { WeatherReportRoutingModule } from './weather-conditions-report-routing.module';

@NgModule({
  declarations: [WeatherConditionsReportComponent],
  imports: [
    CommonModule,
    NgxSpinnerModule,
    WeatherReportRoutingModule,
    NguiDatetimePickerModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class WeatherConditionsReportModule { }
