import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { WeatherConditionsReportComponent } from './weather-conditions-report.component';

const routes: Routes = [
    {path:'',component:WeatherConditionsReportComponent}
]

@NgModule({
    imports: [RouterModule.forChild(routes),FormsModule],
    exports: [RouterModule,CommonModule],
    declarations: [],
})
export class WeatherReportRoutingModule { }