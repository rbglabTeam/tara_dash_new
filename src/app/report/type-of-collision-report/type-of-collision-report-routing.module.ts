import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TypeOfCollisionReportComponent } from './type-of-collision-report.component';

const routes: Routes = [
    {path:'',component:TypeOfCollisionReportComponent}
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    declarations: [],
})
export class TypeCollisionRoutingModule { }