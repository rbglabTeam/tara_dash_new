import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TypeOfCollisionReportComponent } from './type-of-collision-report.component';
import { TypeCollisionRoutingModule } from './type-of-collision-report-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NguiDatetimePickerModule } from '@ngui/datetime-picker';
import { NgxSpinnerModule } from 'ngx-spinner';

@NgModule({
  declarations: [TypeOfCollisionReportComponent],
  imports: [
    CommonModule,TypeCollisionRoutingModule,FormsModule,ReactiveFormsModule,NguiDatetimePickerModule,NgxSpinnerModule
  ],
  schemas:[
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class TypeOfCollisionReportModule { }
