import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TypeOfVehicleReportComponent } from './type-of-vehicle-report.component';

const routes: Routes = [
    {path:'',component:TypeOfVehicleReportComponent}
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    declarations: [],
})
export class TypeOfVechileRoutingModule { }