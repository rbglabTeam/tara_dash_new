import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TypeOfVehicleReportComponent } from './type-of-vehicle-report.component';
import { TypeOfVechileRoutingModule } from './type-of-vehicle-report-routing.module';
import { FormsModule } from '@angular/forms';
import { NguiDatetimePickerModule } from '@ngui/datetime-picker';
import { NgxSpinnerModule } from 'ngx-spinner';

@NgModule({
  declarations: [TypeOfVehicleReportComponent],
  imports: [
    CommonModule,
    TypeOfVechileRoutingModule,
    FormsModule,
    NguiDatetimePickerModule,
    NgxSpinnerModule
  ],
  schemas:[
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class TypeOfVehicleReportModule { }
