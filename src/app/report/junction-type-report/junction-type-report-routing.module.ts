import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { JunctionTypeReportComponent } from './junction-type-report.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

const routes: Routes = [
    {path:'',component:JunctionTypeReportComponent}
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule,FormsModule,ReactiveFormsModule],
    declarations: [],
})
export class JunctionTypeRoutingModule { }