import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import swal from 'sweetalert';
import { NgxSpinnerService } from 'ngx-spinner';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-junction-type-report',
  templateUrl: './junction-type-report.component.html',
  styleUrls: ['./junction-type-report.component.scss']
})
export class JunctionTypeReportComponent implements OnInit {
  model: any = {};
  form: FormGroup;
  error: any = { isError: false, errorMessage: '' };
  isValidDate: any;
  public entries = [];
  allStations: any;
  stationId: any;
  local: any;
  constructor(public api: ApiService,
             private fb: FormBuilder,
             private spinner: NgxSpinnerService,
             public user:UserService
    ) {
      this.user.setAccidents([])
    // this.createForm();
    this.model = {
      StartDate: '',
      EndDate: ''
    };
  }

  ngOnInit() {
    window.scrollTo(0, 0);
    this.getStations();
    this.getLocalstorage();
  }


  getStations() {
    this.api.do('get', 'admin/stations/list', {}, true).then(res => {
      this.allStations = res['data'];
      console.log(res);
    }, err => {
      console.log(err);
    });
  }

  getLocalstorage(): any {
    this.local = JSON.parse(localStorage.getItem('currentUserDash'))
    console.log(this.local)
}

  onSubmit() {
    this.entries = [];
    const packet = {
      station_id: this.stationId,
      from_date: this.model.StartDate,
      to_date: this.model.EndDate
    };
    let currentDate = new Date();
    this.isValidDate = this.validateDates(this.model.StartDate, this.model.EndDate, currentDate);
    if (this.isValidDate == true) {
      this.spinner.show()
      this.api.do('get', 'dash/station/accident/road/junction/report', packet, true).then(res => {
        this.spinner.hide();
        swal({
          title: res['status'],
          icon: res['icon'],
          timer: 1000
        });
        this.entries = res['data'];
        console.log(this.entries);
      });
    }
  }
  validateDates(sDate: string, eDate: string, currentDate: Object) {
    // this.isValidDate = true;
    if ((sDate == '' || eDate == '')) {
      this.error = {
        isError: true, errorMessage: swal({
          text: 'Start date and end date are required',
          icon: 'warning',
          dangerMode: true,
        })
      };
      this.isValidDate = false;
    } else if ((eDate) < (sDate)) {
      this.error = {
        isError: true, errorMessage: swal({
          text: 'To date should be grater then From date.',
          icon: 'warning',
          dangerMode: true,
        })
      };
      this.isValidDate = false;
    } else if ((currentDate) < (sDate) || (currentDate) < (eDate)) {
      this.error = {
        isError: true, errorMessage: swal({
          text: 'Future date should not be selected',
          icon: 'warning',
          dangerMode: true,
        })
      };
      this.isValidDate = false;
    } else {
      this.isValidDate = true;
    }
    return this.isValidDate;
  }

  printOne() {

    var divToPrint = document.getElementById('tableOne');


    var newWin =window.open('', '_blank','scrollbars=yes,menubar=no,toolbar=no,location=no,status=no,titlebar=no,;')

    
    newWin.document.open();

    newWin.document.write('<html><head><style>@page{size: landscape;}</style></head><body onload="window.print()">' + divToPrint.innerHTML+ '</body></html>');

    newWin.document.close();

    setTimeout(function () { newWin.close(); }, 10);
    
  }

  printTwo() {

    var divToPrint = document.getElementById('tableTwo');


    var newWin =window.open('', '_blank','scrollbars=yes,menubar=no,toolbar=no,location=no,status=no,titlebar=no,;')

    
    newWin.document.open();

    newWin.document.write('<html><head><style>@page{size: landscape;}</style></head><body onload="window.print()">' + divToPrint.innerHTML+ '</body></html>');

    newWin.document.close();

    setTimeout(function () { newWin.close(); }, 10);
    
  }



}

