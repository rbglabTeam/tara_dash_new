import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { JunctionTypeReportComponent } from './junction-type-report.component';
import { JunctionTypeRoutingModule } from './junction-type-report-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NguiDatetimePickerModule } from '@ngui/datetime-picker';
import { NgxSpinnerModule } from 'ngx-spinner';

@NgModule({
  declarations: [JunctionTypeReportComponent],
  imports: [
    CommonModule,
    JunctionTypeRoutingModule,
    FormsModule,
    NguiDatetimePickerModule,
    NgxSpinnerModule,
    ReactiveFormsModule
  ],
  schemas:[
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class JunctionTypeReportModule { }
