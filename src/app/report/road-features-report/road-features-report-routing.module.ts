import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RoadFeaturesReportComponent } from './road-features-report.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

const routes: Routes = [
    {path:'',component:RoadFeaturesReportComponent}
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule,FormsModule,ReactiveFormsModule],
    declarations: [],
})
export class RoadFeaturesRoutingModule { }