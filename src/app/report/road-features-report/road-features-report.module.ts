import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RoadFeaturesReportComponent } from './road-features-report.component';
import { RoadFeaturesRoutingModule } from './road-features-report-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NguiDatetimePickerModule } from '@ngui/datetime-picker';
import { NgxSpinnerModule } from 'ngx-spinner';

@NgModule({
  declarations: [RoadFeaturesReportComponent],
  imports: [
    CommonModule,
    RoadFeaturesRoutingModule,
    FormsModule,ReactiveFormsModule,
    NguiDatetimePickerModule,
    NgxSpinnerModule
  ],
  schemas:[
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class RoadFeaturesReportModule { }
