import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RoadEnvironmentRoutingModule } from './road-environment-report-routing.module';
import { RoadEnvironmentReportComponent } from './road-environment-report.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NguiDatetimePickerModule } from '@ngui/datetime-picker';
import { NgxSpinnerModule } from 'ngx-spinner';

@NgModule({
  declarations: [RoadEnvironmentReportComponent],
  imports: [
    CommonModule,
    RoadEnvironmentRoutingModule,
    FormsModule,ReactiveFormsModule,
    NguiDatetimePickerModule,NgxSpinnerModule
  ]
})
export class RoadEnvironmentReportModule { }
