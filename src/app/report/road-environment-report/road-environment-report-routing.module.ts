import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RoadEnvironmentReportComponent } from './road-environment-report.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

const routes: Routes = [
    {path:'',component:RoadEnvironmentReportComponent}
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule,FormsModule],
    declarations: [],
})
export class RoadEnvironmentRoutingModule { }