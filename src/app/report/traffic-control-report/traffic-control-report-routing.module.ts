import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TrafficControlReportComponent } from './traffic-control-report.component';
import { FormsModule } from '@angular/forms';

const routes: Routes = [
    {path:'',component:TrafficControlReportComponent}
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule,FormsModule],
    declarations: [],
})
export class TrafficControlRoutingModule { }