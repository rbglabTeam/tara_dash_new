import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TrafficControlReportComponent } from './traffic-control-report.component';
import { TrafficControlRoutingModule } from './traffic-control-report-routing.module';
import { NguiDatetimePickerModule } from '@ngui/datetime-picker';
import { NgxSpinnerModule } from 'ngx-spinner';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [TrafficControlReportComponent],
  imports: [
    CommonModule,TrafficControlRoutingModule,NguiDatetimePickerModule,NgxSpinnerModule,FormsModule
  ],
  schemas:[
    CUSTOM_ELEMENTS_SCHEMA
    
  ]
})
export class TrafficControlReportModule { }
