import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UseNonuseSafetyReportComponent } from './use-nonuse-safety-report.component';

const routes: Routes = [
    {path:'',component:UseNonuseSafetyReportComponent}
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    declarations: [],
})
export class UseNonuseSafetyRoutingModule { }