import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UseNonuseSafetyRoutingModule } from './use-nonuse-safety-report-routing.module';
import { UseNonuseSafetyReportComponent } from './use-nonuse-safety-report.component';
import { FormsModule } from '@angular/forms';
import { NguiDatetimePickerModule } from '@ngui/datetime-picker';
import { NgxSpinnerModule } from 'ngx-spinner';

@NgModule({
  declarations: [UseNonuseSafetyReportComponent],
  imports: [
    CommonModule,
    UseNonuseSafetyRoutingModule,
    FormsModule,
    NguiDatetimePickerModule,
    NgxSpinnerModule
  ],
  schemas:[
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class UseNonuseSafetyReportModule { }
