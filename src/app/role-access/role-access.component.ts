import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { UserService } from '../services/user.service';
import { ApiService } from '../services/api.service';
import swal from 'sweetalert';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-role-access',
  templateUrl: './role-access.component.html',
  styleUrls: ['./role-access.component.scss']
})
export class RoleAccessComponent implements OnInit {

  model: any = {};
  data = {}
  allStations = [];

  filterData: {};
  constructor(private formBuilder: FormBuilder, private route: ActivatedRoute, public spinner: NgxSpinnerService,
    public user: UserService, public api: ApiService) {
    this.user.setAccidents([])
    this.model = {
      name: "",
      mail: "",
      phNumber: "",
      token: this.api.token,

      data: {
        state: '',
        district: "",
        zoneCode: ''
      }

    }
  }

  ngOnInit() {
    this.getStations();
    window.scrollTo(0, 0);
  }
  onSubmit() {

    this.spinner.show()
    this.api.do('post', 'admin/station/create', this.model, true).then(res => {
      this.spinner.hide()
      if (res['password']) {
        swal({
          title: res['password'],
          text: "Please check registered email for password",
          icon: res['icon'],
        });
        console.log(res);
        this.clearForm();
      } else {
        swal({
          title: res['statusCode'],
          text: res['status'],
          icon: res['icon'],
        });
      }
    }, err => {
      console.log(err);
    });

  }
  getStations() {
    this.spinner.show()
    this.api.do('get', 'admin/stations/list', {}, true).then(res => {
      this.spinner.hide()
      this.allStations = res['data']
      console.log(res);
    }, err => {
      console.log(err);
    })
  }
  clearForm() {
    this.model = {
      name: "",
      mail: "",
      phNumber: "",
      data: {
        district: "",
        zoneCode: "",
        state: ""
      }

    }
  }

  search(term: string) {
    if (!term) {
      this.filterData = this.allStations;
    } else {
      this.filterData = this.allStations.filter(x =>
        x.name.trim().toLowerCase().includes(term.trim().toLowerCase()),
      );
      console.log(this.filterData);

    }
  }



  districts = [
    'Kanchipuram',
    'Tiruvallur',
    'Cuddalore',
    'Villupuram',
    'Vellore',
    'Tiruvannamalai',
    'Salem',
    'Namakkal',
    'Dharmapuri',
    'Erode',
    'Coimbatore',
    'The Nilgiris',
    'Thanjavur',
    'Nagapattinam',
    'Tiruvarur',
    'Tiruchirappalli',
    'Karur',
    'Perambalur',
    'Pudukkottai',
    'Madurai',
    'Theni',
    'Dindigul',
    'Ramanathapuram',
    'Virudhunagar',
    'Sivagangai',
    'Tirunelveli',
    'Thoothukkudi',
    'Kanniyakumari',
    'Krishnagiri',
    'Ariyalur',
    'Tiruppur',
    'Chennai'
  ];
  states = [
    "State1",
    "State2"
  ]
  zoneCode = [
    "KP01", "KP02", "KP03", "KP04"
  ]

}
