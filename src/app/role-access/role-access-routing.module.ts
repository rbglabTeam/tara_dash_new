import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NguiDatetimePickerModule } from '@ngui/datetime-picker';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { RoleAccessComponent } from './role-access.component';

const routes: Routes = [
    {path:'',component:RoleAccessComponent}
]

@NgModule({
    imports: [RouterModule.forChild(routes),FormsModule,NguiDatetimePickerModule],
    exports: [RouterModule,CommonModule],
    declarations: [],
    schemas:[
        CUSTOM_ELEMENTS_SCHEMA
      ],
})
export class RoleAccessRoutingModule { }