import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CommonDashComponent} from './common-dash.component'
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

const routes: Routes = [
    {path:'',component:CommonDashComponent},
    // {path:'dashStation',loadChildren:'../app/admin/dash-station/dash-station.module#DashStationModule'},
    // {path:'dashUser',loadChildren:'../app/station/dash-user/dash-user.module#DashUserModule'}

]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule,CommonModule,ReactiveFormsModule],
    declarations: [],
})
export class CommonDashRoutingModule { }