import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from '../services/user.service';
import { NgxSpinnerService } from 'ngx-spinner';


@Component({
  selector: 'app-common-dash',
  templateUrl: './common-dash.component.html',
  styleUrls: ['./common-dash.component.scss']
})
export class CommonDashComponent implements OnInit {


  public local

  constructor( public spinner: NgxSpinnerService) { }

  ngOnInit() {
    this.getLocalstorage()
  }


  getLocalstorage(): any {

    this.local = JSON.parse(localStorage.getItem('currentUserDash'))

}
}
