import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {CommonDashRoutingModule} from './common-dash-routing.module';
import { CommonDashComponent } from './common-dash.component';
import { DashStationComponent } from '../admin/dash-station/dash-station.component';
import { DashUserComponent } from '../station/dash-user/dash-user.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxSpinnerModule } from 'ngx-spinner';
import { NguiDatetimePickerModule } from '@ngui/datetime-picker';


@NgModule({
  declarations: [CommonDashComponent,DashStationComponent,DashUserComponent],
  imports: [
    CommonModule,
    CommonDashRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgxSpinnerModule,
    NguiDatetimePickerModule
  ]
})
export class CommonDashModule { }
