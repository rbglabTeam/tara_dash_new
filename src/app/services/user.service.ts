import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiService } from '../services/api.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  type = '';
  myDate: Date;
  entries = [];
  _id = '';
  selectedDate: any;
  accidents: any;
  constructor(private http: HttpClient, public api: ApiService, private spinner: NgxSpinnerService) { }



  // getAccident(_id) {
  //   console.log();
  //   let packet = {
  //     accident_id: _id
  //   }

  //   this.api.do('get', 'dash/admin/accident', packet, true).then(res => {
  //     if (res['code'] === 200) {
  //       // this.accident_id = res['data']['data'];
  //       this._id = res['data']['_id'];
  //     }
  //     console.log(res);
  //   }, err => {
  //     console.log(err);
  //   })

  // }

  getAccidents() {
    return this.accidents
  }
  setAccidents(value) {
    this.accidents = value
  }
  onLoadingSpinner(){
    this.spinner.show();
  }
  ongetResult(){
    this.spinner.hide();
  }




}






