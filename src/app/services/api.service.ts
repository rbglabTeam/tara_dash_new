import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  public token = ''
  jwt: string;
  // public serverUrl = 'https://secret-savannah-48267.herokuapp.com/'
  // public serverUrl = 'http://localhost:9001/'
  // public serverUrl = 'http://10.42.91.11:9001/'
  // public serverUrl = 'http://192.168.0.102:9001/'
  // public serverUrl = 'http://192.168.2.37:9001/'
  public serverUrl = 'http://localhost:9001/'

  /**
  * AWS Server
  *
  */
  // public serverUrl = 'http://13.234.52.55:9002/'
  constructor(public http: HttpClient) {

    // this.token=this.jwt['token']

  }

  public async do(
    method?: string,
    url?: string,
    params?: any,
    loading?: boolean,
    duration?: number
  ) {
    await this.getToken()
    url = this.serverUrl + url
    switch (method) {
      case 'get':
        if (this.token) params.token = this.token;
        let query = url + '?';
        Object.keys(params).forEach(element => {
          query += element + '=' + params[element] + '&';
        });
        // if (loading) this.loading.show();
        return new Promise((resolve, reject) => {
          this.http.get(query).subscribe(res => {
            // if (loading) this.loading.hide();
            // if (res['status'])
            //   this.alerts.showToast(res['status'], 2000)
            resolve(res);
          },
            err => {
              console.log(err);
              reject(err);
            }
          );
        });
        break;
      case 'post':
        if (this.token) params.token = this.token;
        // if (loading) this.loading.show();
        return new Promise((resolve, reject) => {
          this.http.post(url, params).subscribe(
            res => {
              // if (loading) this.loading.hide();
              // if (res['status'])
              //   this.alerts.showToast(res['status'], 2000)
              resolve(res);
            },
            err => {
              console.log(err);
              reject(err);
            }
          );
        });
        break;
    }
  }
  printFunction(_id) {
    try {
      let value = {
        id: _id
      };
      return new Promise(resolve => {
        this.http.post<{ data: any }>(this.serverUrl + 'api/pdf', value).subscribe(response => {
          resolve(response.data);
        })
      })
    } catch (error) {

    }
  }

  async getToken() {
    let token = await JSON.parse(localStorage.getItem('currentUserDash'))
    if (token) {
      this.token = token['token']
      return this.token
    } else {
      return this.token = '';
    }
  }
}
