import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from './login.component'
import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';


const routes: Routes = [
    {path:'',component:LoginComponent}
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule,ReactiveFormsModule, CommonModule ],
    declarations: [],
})
export class LoginRoutingModule { }