import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from '../services/api.service';
import { UserService } from '../services/user.service';
import swal from 'sweetalert';
import { NgxSpinnerService } from 'ngx-spinner';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  login: any = {};
  type: any = { 'roleType': 'Station' };

  options = ['Station', 'Admin'];
  registerForm: FormGroup;
  submitted = false;


  constructor(
    private route: ActivatedRoute,
    private router: Router,
    public api: ApiService,
    private spinner: NgxSpinnerService,
    private formBuilder: FormBuilder,
    public user: UserService) {
    this.user.setAccidents([])
  }

  ngOnInit() {
    window.scrollTo(0, 0);
    this.initializeForm();

  }

  initializeForm() {
    this.registerForm = this.formBuilder.group({
      mail: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]]
    });
  }

  get f() { return this.registerForm.controls; }

  onSubmit(val) {

    this.submitted = true;

    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    }
    this.spinner.show()
    this.api.do('post', 'api/login/dashuser', val, true).then(res => {
      console.log(res);
      if (res['token']) {
        swal({
          title: 'Success',
          icon: 'success',

        });
        localStorage.setItem('currentUserDash', JSON.stringify({ token: res['token'], roleType: res['role'], mail: res['mail'], name: res['name'] }));
        this.api.token = res['token']
        this.user.type = res['role']
        if (this.api.token)
        this.spinner.hide();
          this.router.navigate(['landing'], { replaceUrl: true });

        console.log(res)
      } else {
        this.spinner.hide();
        swal({
          title: res['status'],
          icon: res['icon'],
          timer: 1000
        });
      }
    })


  }

  // onSumit() {
  //   this.spinner.show();
  //   if (this.type.roleType === "Station") {

  //     this.api.do('post', 'api/station', this.login, true).then(res => {
  //       this.spinner.hide();
  //       if (res['token']) {
  //         swal({
  //           title: 'Success',
  //           icon: 'success',

  //         });
  //         localStorage.setItem('currentUserDash', JSON.stringify({ token: res['token'], roleType: this.type.roleType,mail:res['mail'],name:res['name'] }));
  //         this.api.token = res['token']
  //         this.user.type = "Station"
  //         if (this.api.token)
  //           this.router.navigate(['landing'], { replaceUrl: true });

  //         console.log(res)
  //       } else {
  //         swal({
  //           title: res['status'],
  //           icon: res['icon'],
  //           timer: 1000
  //         });
  //       }
  //     }, err => {
  //       console.log(err)
  //     })
  //   } else if (this.type.roleType === "Admin") {
  //     this.api.do('post', 'api/admin', this.login, true).then(res => {
  //       this.spinner.hide();
  //       swal({
  //         title: res['status'],
  //         icon: res['icon'],

  //         timer: 1000
  //       });
  //       localStorage.setItem('currentUserDash', JSON.stringify({ token: res['token'], roleType: this.type.roleType ,mail:res['mail'],name:res['name']}));
  //       this.api.token = res['token']
  //       console.log(this.api.token, 'token');
  //       this.user.type = "Admin"
  //       if (this.api.token) this.router.navigate(['landing'], { replaceUrl: true });
  //       console.log(res);
  //     }, err => {
  //       console.log(err);
  //     })
  //   }
  // }


  getUser() {
    this.api.do('get', '', {}, true).then(res => {
      console.log(res)
    }, err => {
      console.log(err)
    })
  }


}
