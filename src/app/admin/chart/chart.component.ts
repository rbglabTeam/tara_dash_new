import { Component, OnInit } from '@angular/core';
import * as CanvasJS from '../../../assets/canvasjs/canvasjs.min';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss']
})
export class ChartComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    this.initialchart()
    this.initialChartTwo()
  }

  initialchart(){
    
  
    var chart = new CanvasJS.Chart("multiAxes", {
      exportEnabled: true,
      animationEnabled: true,
      title:{
        text: "Dharmapuri TARA Entry Status"
      },
      // subtitles: [{
      //   text: "Click Legend to Hide or Unhide Data Series"
      // }], 
      axisX: {
        title: "States"
      },
      axisY: {
        title: "Oil Filter - Units",
        titleFontColor: "#4F81BC",
        lineColor: "#4F81BC",
        labelFontColor: "#4F81BC",
        tickColor: "#4F81BC"
      },
      axisY2: {
        title: "Clutch - Units",
        titleFontColor: "#C0504E",
        lineColor: "#C0504E",
        labelFontColor: "#C0504E",
        tickColor: "#C0504E"
      },
      toolTip: {
        shared: true
      },
      legend: {
        cursor: "pointer",
        itemclick: toggleDataSeries
      },
      data: [{
        type: "column",
        name: "Oil Filter",
        showInLegend: true,      
        yValueFormatString: "#,##0.# Units",
        dataPoints: [
          { label: "Date1",  y: 19034.5 },
          { label: "Date2", y: 20015 },
          { label: "Date3", y: 25342 },
          { label: "Date4",  y: 20088 },
          { label: "Date5",  y: 28234 }
        ]
      },
      {
        type: "column",
        name: "Clutch",
        axisYType: "secondary",
        showInLegend: true,
        yValueFormatString: "#,##0.# Units",
        dataPoints: [
          { label: "Date1", y: 210.5 },
          { label: "Date2", y: 135 },
          { label: "Date3", y: 425 },
          { label: "Date4", y: 130 },
          { label: "Date5", y: 528 }
        ]
      }]
    });
    chart.render();
    
    function toggleDataSeries(e) {
      if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
        e.dataSeries.visible = false;
      } else {
        e.dataSeries.visible = true;
      }
      e.chart.render();
      }
    }

    initialChartTwo(){
      let chart = new CanvasJS.Chart("singleAxes", {
        animationEnabled: true,
        exportEnabled: true,
        title: {
          text: "Dharmapuri TARA Entries"
        },
        data: [{
          type: "column",
          dataPoints: [
            { y: 71, label: "Date1" ,color:"blue"},
            { y: 55, label: "Date2" ,color:"blue" },
            { y: 50, label: "Date3"  ,color:"blue"},
            { y: 65, label: "Date4"  ,color:"blue"},
            { y: 95, label: "Date5"  ,color:"blue"},
            // { y: 68, label: "Pears"  ,color:"blue"},
            // { y: 28, label: "Grapes"  ,color:"blue"},
            // { y: 34, label: "Lychee"  ,color:"blue"},
            // { y: 14, label: "Jackfruit"  ,color:"blue"}
          ]
        }]
      });
        
      chart.render();
    }
   
}
