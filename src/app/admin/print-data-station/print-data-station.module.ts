import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PrintDataStationComponent } from './print-data-station.component';
import { PrintDataStationRoutingModule } from './print-data-station-routing.module';
import { NgxSpinnerModule } from 'ngx-spinner';

@NgModule({
  declarations: [PrintDataStationComponent],
  imports: [
    CommonModule,
    PrintDataStationRoutingModule,
    NgxSpinnerModule
  ]
})
export class PrintDataStationModule { }
