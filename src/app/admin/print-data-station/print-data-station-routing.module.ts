import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { PrintDataStationComponent } from './print-data-station.component';

const routes: Routes = [
    {path:'',component:PrintDataStationComponent}
]

@NgModule({
    imports: [RouterModule.forChild(routes),FormsModule],
    exports: [RouterModule,CommonModule],
    declarations: [],
})
export class PrintDataStationRoutingModule { }