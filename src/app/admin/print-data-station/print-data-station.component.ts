import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { ApiService } from '../../services/api.service';
import * as jspdf from 'jspdf';
import { ExportAsService, ExportAsConfig } from 'ngx-export-as';
import { NgxSpinnerService } from 'ngx-spinner';
@Component({
  selector: 'app-print-data-station',
  templateUrl: './print-data-station.component.html',
  styleUrls: ['./print-data-station.component.scss']
})
export class PrintDataStationComponent implements OnInit {
  accidentData: [];
  _id = '';
  exportAsConfig: ExportAsConfig = {
    type: 'csv', // the type you want to download
    elementId: 'contentToConvert', // the id of html/table element
  }
  constructor(public route: ActivatedRoute,
    public router:Router,
    private spinner: NgxSpinnerService,
    public api: ApiService, private exportAsService: ExportAsService) {
    this._id = this.route.snapshot.paramMap.get("id");
    if (this._id)
      this.getAccident();
    else {
      console.log("llll")
    }
  }

  ngOnInit() {
    window.scrollTo(0, 0);
  }
  get getUrl() {
    return this.api.serverUrl + 'media/image?image='
    // return "https://secret-savannah-48267.herokuapp.com/media/image?image="
  }
  get getAudio() {
    return this.api.serverUrl + 'media/audio?audio='
    // return "https://secret-savannah-48267.herokuapp.com/media/audio?audio="
  }
  get getVideo() {
    return this.api.serverUrl + 'media/video?video='
    // return "https://secret-savannah-48267.herokuapp.com/media/video?video="
  }

  getAccident() {
    console.log();
    let packet = {
      accident_id: this._id
    }

    this.api.do('get', 'admin/accident', packet, true).then(res => {

      if (res['code'] === 200) {
        this.accidentData = res["data"]["data"];
      }
      console.log(res);
    }, err => {
      console.log(err);
    });

  }
  sendMail(){
    this.router.navigate(['/landing/sendMail'])
  }
  // printPDF() {

  //   var divToPrint = document.getElementById('toPrint');

  //   var newWin = window.open('', 'Print-Window');

  //   newWin.document.open();

  //   newWin.document.write('<html><body onload="window.print()">' + divToPrint.innerHTML + '</body></html>');

  //   newWin.document.close();

  //   setTimeout(function () { newWin.close(); }, 10);
  // }

  printFunctionApi() {
    this.spinner.show();
    this.api.printFunction(this._id).then(res => {
      this.spinner.hide();
      const bearer = 'data:application/pdf;base64,'
      const pdfInBase64 = bearer + res;
      const linkSource = pdfInBase64;
      const downloadLink = document.createElement("a");
      const fileName = `${this._id}.pdf`;

      downloadLink.href = linkSource;
      downloadLink.download = fileName;

      downloadLink.click();

    })
  }
  public captureScreen() {
    var table = document.getElementById("toPrint");
    var currentHeight = 20
    var max = 300
    let pdf = new jspdf('p', 'mm', 'a4'); // A4 size page of PDF
    pdf.text(60, 10, 'Transportation Accident Report');
    pdf.setFontSize(12);
    pdf.setFontStyle('Arial, Helvetica, sans-serif');
    for (var i = 1; i < table['rows'].length; i++) {

      if ((currentHeight + 30) > max) {
        pdf.addPage()
        currentHeight = 20
      }
      var textvalue = table['rows'][i].cells[0].innerHTML + ' ----- ' + table['rows'][i].cells[1].innerHTML
      if (textvalue.includes('http') || textvalue.includes('"ng-reflect-ng-if')) {
        console.log('media');
      }
      else {
        pdf.text(20, currentHeight, textvalue);
        currentHeight = currentHeight + 10;
      }
    }
    pdf.save('TARA Case Report.pdf');
  }
}


