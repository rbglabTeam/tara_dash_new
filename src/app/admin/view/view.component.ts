import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { UserService } from '../../services/user.service';
import { NgxSpinnerService } from 'ngx-spinner';
import swal from 'sweetalert';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class ViewComponent implements OnInit {
  allStations = [];
  stationId = "";
  entries = [];
  myDate = new Date();
  model = {
    StartDate: "",
    EndDate: ""
  }
  error: any = { isError: false, errorMessage: '' };
  isValidDate: any;
  config: any;


  entry: [];
  constructor(public api: ApiService,
    public user: UserService, private spinner: NgxSpinnerService) {
    this.entries = this.user.getAccidents();
    this.config = {
      itemsPerPage: 10,
      currentPage: 1,
    };
  }

  ngOnInit() {
    this.getStations();
    window.scrollTo(0, 0);
  }
  getStations() {
    this.api.do('get', 'admin/stations/list', {}, true).then(res => {
      this.allStations = res['data'];
      console.log(res);
    }, err => {
      console.log(err);
    });
  }
  async clickme(_) {
    this.entries = [];
    let packet = {
      station_id: this.stationId,
      from_date: this.model.StartDate,
      to_date: this.model.EndDate
    }
    console.log("sucess");
    let currentDate = new Date();
    this.isValidDate = await this.validateDates(this.model.StartDate, this.model.EndDate, currentDate);
    if (this.isValidDate == true) {
      this.spinner.show();
      this.api.do('get', 'admin/accident/list', packet, true).then(res => {
        this.spinner.hide();
        swal({
          title: res['status'],
          icon: res['icon'],

          timer: 1000
        });
        if (res['code'] === 200) {
          let data = res['data'];
          data.forEach(element => {
            let modified = {
              date: element.duration_date_time.slice(0, 10),
              hourMinute: element.hourMinute,
              _id: element._id
            }
            this.entries.push(modified)
          });
          this.user.setAccidents(this.entries)
        }
        console.log(res);
      }, err => {
        console.log(err);
      });
    }

  }
  // paginationevent
  pageChanged(event) {
    this.config.currentPage = event;
  }

  validateDates(sDate: string, eDate: string, currentDate: Object) {
    // this.isValidDate = true;
    if ((sDate == "" || eDate == "")) {
      this.error = {
        isError: true, errorMessage: swal({
          text: "Start date and end date are required",
          icon: "warning",
          dangerMode: true,
        })
      };
      this.isValidDate = false;
    }

    else if ((eDate) < (sDate)) {
      this.error = {
        isError: true, errorMessage: swal({
          text: "To date should be grater then From date.",
          icon: "warning",
          dangerMode: true,
        })
      };
      this.isValidDate = false;
    }
    else if ((currentDate) < (sDate) || (currentDate) < (eDate)) {
      this.error = {
        isError: true, errorMessage: swal({
          text: "Future date should not be selected",
          icon: "warning",
          dangerMode: true,
        })
      };
      this.isValidDate = false;
    }
    else {
      this.isValidDate = true
    }
    return this.isValidDate;
  }

}














