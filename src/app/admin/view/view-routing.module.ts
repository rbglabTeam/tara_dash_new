import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NguiDatetimePickerModule } from '@ngui/datetime-picker';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ViewComponent } from './view.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { ViewModule } from './view.module';


const routes: Routes = [
    {path:'',component:ViewComponent}
]

@NgModule({
    imports: [RouterModule.forChild(routes),FormsModule,ReactiveFormsModule,NguiDatetimePickerModule],
    exports: [RouterModule,CommonModule,NgxPaginationModule],
    declarations: [],
})
export class ViewRoutingModule { }