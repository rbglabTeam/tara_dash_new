import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NguiDatetimePickerModule } from '@ngui/datetime-picker';
import { NgxSpinnerService, NgxSpinnerModule } from 'ngx-spinner';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ViewComponent } from './view.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { ViewRoutingModule } from './view-routing.module';



@NgModule({
  declarations: [ViewComponent],
  imports: [
    CommonModule,
    ViewRoutingModule,
    NguiDatetimePickerModule,
    NgxSpinnerModule,
    NgxPaginationModule,
    FormsModule
  ],
  schemas:[
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class ViewModule { }
