import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashStationRoutingModule } from './dash-station-routing.module';
import { DashStationComponent } from './dash-station.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NguiDatetimePickerModule } from '@ngui/datetime-picker';
import { NgxSpinnerService, NgxSpinnerModule } from 'ngx-spinner';

@NgModule({
  declarations: [DashStationComponent],
  imports: [
    CommonModule,
    NguiDatetimePickerModule,
    ReactiveFormsModule,
    DashStationRoutingModule,
    NguiDatetimePickerModule,
    NgxSpinnerModule,
  ]
})
export class DashStationModule { }
