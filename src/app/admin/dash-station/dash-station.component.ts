import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { NgxSpinnerService } from 'ngx-spinner';
import swal from 'sweetalert';
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import 'datatables.net-buttons-bs';
import 'datatables.net-buttons/js/dataTables.buttons.min';
import 'datatables.net-buttons/js/buttons.flash.min';
import 'datatables.net-buttons/js/buttons.html5.min';
import 'datatables.net-buttons/js/buttons.html5.js'
import 'datatables.net-buttons/js/buttons.print.js'
import { UserService } from 'src/app/services/user.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as _ from 'lodash';

@Component({
  selector: 'app-dash-station',
  templateUrl: './dash-station.component.html',
  styleUrls: ['./dash-station.component.scss']
})
export class DashStationComponent implements OnInit {
  stationDashForm: FormGroup;
  submitted = false;
  allStations = [];
  stationId = "";
  numbercount = "";
  entries = [];
  currentDateCaseCount: any;
  myDate = new Date();
  obj = [{ 'station_mail': '', 'individual_station_case_count': '', 'current_date_case_count': 0 }];
  current_case_count = { 'current_date_case_count': 0 };
  model = {
    StartDate: "",
    EndDate: ""
  }
  error: any = { isError: false, errorMessage: '' };
  isValidDate: any;
  dashDetails: any;
  map = new Map();
  dateData = new Map();
  datas:any
  constructor(
    public api: ApiService,
    public user: UserService,
    private formBuilder: FormBuilder,
    public spinner: NgxSpinnerService) {
    this.user.setAccidents([])
  }

  ngOnInit() {
    this.datas=
      [
       { "data":  "Tiger Nixon"},
       { "data":  "qediger Nixon"},
       { "data":  "qediger Nixon"},
       { "data":  "qediger Nixon"},
       { "data":  "qediger Nixon"},
       { "data":  "qediger Nixon"},
       { "data":  "qediger Nixon"},
       { "data":  "qediger Nixon"},
       { "data":  "qediger Nixon"},
       { "data":  "qediger Nixon"},
       { "data":  "qediger Nixon"}
      ],
    
    
    this.user.onLoadingSpinner();
    this.getDashdetails()
    window.scrollTo(0, 0);
    this.initializeForm()

    $(document).ready(function() {
      $('#example').DataTable( {
      } );
  } );

  }

  initializeForm() {
    this.stationDashForm = this.formBuilder.group({
      StartDate: ['', [Validators.required]],
      EndDate: ['', [Validators.required]]
    });
  }

  get f() { return this.stationDashForm.controls; }

 async onSubmit(val) {
    this.submitted = true;

    // stop here if form is invalid
    if (this.stationDashForm.invalid) {
      return;
    }


    this.spinner.show()
    let packet = {
      from_date: val.StartDate,
      to_date: val.EndDate
    }
    console.log("sucess");
    let currentDate = new Date();
    this.isValidDate = this.validateDates(val.StartDate, val.EndDate, currentDate);
    if (this.isValidDate == true) {
      this.api.do('post', 'admin/dashboard/stations/case/list', packet, true).then(res => {
        this.spinner.hide()
        if (res['code'] === 200) {
          this.dateData = res['data'];
          if (this.dateData) {
            this.onCurrentDateRefresh();
            this.updateDateDetails(this.dateData);

          }

        } else {
          this.onCurrentDateRefresh();
          swal({
            title: res['status'],
            icon: res['icon'],
            timer: 1000
          });
        }
        console.log(res);
      }, err => {
        console.log(err);
      });
    }

  }
 async getStations() {
   
    this.spinner.show()
    this.api.do('get', 'admin/stations/list', {}, true).then(res => {
      this.allStations = res['data']
      console.log(res);
      this.spinner.hide()
    }, err => {
      console.log(err);
      this.spinner.hide()

    })
  }
  getDashdetails() {

    this.spinner.show()


    this.api.do('get', 'admin/dashboard/stations/details', {}, true).then(res => {


      this.dashDetails = res['data'];
      this.map = res['data']['station_name'];
      // console.log('response',this.map)

      this.spinner.hide()
    }, err => {
      console.log(err);

    })
  }
  /**
   *
   * @param dataobject update the get data from both two dates
   */
  updateDateDetails(dataobject) {
    debugger;

    var value = _.assign(this.map, _.toPlainObject(new this.Foo(this.map, dataobject)));
  }
  Foo(object, dataobject) {
    object.forEach(element => {
      // element.current_date_case_count=5
      dataobject.forEach(data => {
        // element.current_date_case_count=5
        if (element.station_mail === data.station_mail) {
          element.current_date_case_count = data.current_date_case_count;
        }
      });
    });
  }
  /**
   * Get data between two dates!
   */
  clickme() {
    this.spinner.show()
    let packet = {
      from_date: this.model.StartDate,
      to_date: this.model.EndDate
    }
    console.log("sucess");
    let currentDate = new Date();
    this.isValidDate = this.validateDates(this.model.StartDate, this.model.EndDate, currentDate);
    if (this.isValidDate == true) {
      this.api.do('post', 'admin/dashboard/stations/case/list', packet, true).then(res => {
        this.spinner.hide()
        if (res['code'] === 200) {
          this.dateData = res['data'];
          if (this.dateData) {
            this.onCurrentDateRefresh();
            this.updateDateDetails(this.dateData);
            this.spinner.hide()

          }

        } else {
          this.onCurrentDateRefresh();
          this.spinner.hide()
          swal({
            title: res['status'],
            icon: res['icon'],
            timer: 1000
          });
        }
        console.log(res);
      }, err => {
        console.log(err);
      });
    }
  }
  /**
   * reset current date case count property value!
   */
  onCurrentDateRefresh() {
    this.dashDetails.station_name.forEach(element => {
      element.current_date_case_count = 0;
    });
  }

  validateDates(sDate: string, eDate: string, currentDate: Object) {
    // this.isValidDate = true;
    if ((sDate == "" || eDate == "")) {
      this.error = {
        isError: true, errorMessage: swal({
          text: "Start date and end date are required",
          icon: "warning",
          dangerMode: true,
        })
      };
      this.isValidDate = false;
    }

    else if ((eDate) < (sDate)) {
      this.error = {
        isError: true, errorMessage: swal({
          text: "To date should be grater then From date.",
          icon: "warning",
          dangerMode: true,
        })
      };
      this.isValidDate = false;
    }
    else if ((currentDate) < (sDate) || (currentDate) < (eDate)) {
      this.error = {
        isError: true, errorMessage: swal({
          text: "Future date should not be selected",
          icon: "warning",
          dangerMode: true,
        })
      };
      this.isValidDate = false;
    }
    else {
      this.isValidDate = true
    }
    return this.isValidDate;
  }

  

 
  

}
