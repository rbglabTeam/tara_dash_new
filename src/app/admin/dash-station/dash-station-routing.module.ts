import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashStationComponent } from './dash-station.component';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NguiDatetimePickerModule } from '@ngui/datetime-picker';

const routes: Routes = [
    {path:'dashStation',component:DashStationComponent}
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule,ReactiveFormsModule,CommonModule],
    declarations: [],
})
export class DashStationRoutingModule { }