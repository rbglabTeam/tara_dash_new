import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../../services/api.service';
import { NgxSpinnerService } from 'ngx-spinner';
import swal from 'sweetalert';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-quick-look',
  templateUrl: './quick-look.component.html',
  styleUrls: ['./quick-look.component.scss']
})
export class QuickLookComponent implements OnInit {
  allStations = [];
  stationId = "";
  entries = [];
  myDate = new Date();
  model = {
    StartDate: "",
    EndDate: ""
  }
  error: any = { isError: false, errorMessage: '' };
  isValidDate: any;
  
  constructor(
    private route: ActivatedRoute,
    public api: ApiService,
    public user:UserService,
    private spinner: NgxSpinnerService) {
      this.user.setAccidents([])
     }

  ngOnInit() {
    this.getStations()
    window.scrollTo(0, 0);
  }
  getStations() {
    this.api.do('get', 'admin/stations/list', {}, true).then(res => {
      this.allStations = res['data']
      console.log(res);
    }, err => {
      console.log(err);
    })
  }
  clickme(_) {

    let packet = {
      station_id: this.stationId,
      from_date: this.model.StartDate,
      to_date: this.model.EndDate
    }
    let currentDate = new Date();
    console.log("sucess");
    this.isValidDate = this.validateDates(this.model.StartDate, this.model.EndDate, currentDate);
    if (this.isValidDate == true){
      this.spinner.show();
      this.api.do('get', 'admin/accident/list', packet, true).then(res => {
        this.spinner.hide();
        if (res['code'] === 200) {
          this.entries = res['data'];
        }
        console.log(res);
      }, err => {
        console.log(err);
      });
    }

  }

  validateDates(sDate: string, eDate: string, currentDate: Object) {
    // this.isValidDate = true;
    if ((sDate == "" || eDate == "")) {
      this.error = {
        isError: true, errorMessage: swal({
          text: "Start date and end date are required",
          icon: "warning",
          dangerMode: true,
        })
      };
      this.isValidDate = false;
    }

    else if ((eDate) < (sDate)) {
      this.error = {
        isError: true, errorMessage: swal({
          text: "To date should be grater then From date.",
          icon: "warning",
          dangerMode: true,
        })
      };
      this.isValidDate = false;
    }
    else if ((currentDate) < (sDate) || (currentDate) < (eDate)) {
      this.error = {
        isError: true, errorMessage: swal({
          text: "Future date should not be selected",
          icon: "warning",
          dangerMode: true,
        })
      };
      this.isValidDate = false;
    }
    else {
      this.isValidDate = true
    }
    return this.isValidDate;
  }

}
