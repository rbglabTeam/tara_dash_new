import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { UserService } from '../../services/user.service';
import { ApiService } from '../../services/api.service';
import swal from 'sweetalert';
import { NgxSpinnerModule, NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent implements OnInit {
  model: any = {};
  data = {}
  allStations = [];
  districts = [
    'Kanchipuram',
    'Tiruvallur',
    'Cuddalore',
    'Villupuram',
    'Vellore',
    'Tiruvannamalai',
    'Salem',
    'Namakkal',
    'Dharmapuri',
    'Erode',
    'Coimbatore',
    'The Nilgiris',
    'Thanjavur',
    'Nagapattinam',
    'Tiruvarur',
    'Tiruchirappalli',
    'Karur',
    'Perambalur',
    'Pudukkottai',
    'Madurai',
    'Theni',
    'Dindigul',
    'Ramanathapuram',
    'Virudhunagar',
    'Sivagangai',
    'Tirunelveli',
    'Thoothukkudi',
    'Kanniyakumari',
    'Krishnagiri',
    'Ariyalur',
    'Tiruppur',
    'Chennai'
  ];
  states = [
    "State1",
    "State2"
  ]
  constructor(private formBuilder: FormBuilder, private route: ActivatedRoute,public spinner:NgxSpinnerService,
    public user: UserService, public api: ApiService) {
    this.user.setAccidents([])
    this.model = {
      name: "",
      mail: "",
      phNumber: "",
      token: this.api.token,

      data: {
        state: '',
        district: "",
        city: "",
        latitute: "",
        longitude: ""
      }

    }
  }

  ngOnInit() {
    this.getStations();
    window.scrollTo(0, 0);
  }
  onSubmit() {

  this.spinner.show()
    this.api.do('post', 'admin/station/create', this.model, true).then(res => {
      this.spinner.hide()
      if (res['password']) {
        swal({
          title: res['password'],
          text: "Please check registered email for password",
          icon: res['icon'],
        });
        console.log(res);
        this.clearForm();
      } else {
        swal({
          title: res['statusCode'],
          text: res['status'],
          icon: res['icon'],
        });
      }
    }, err => {
      console.log(err);
    });

  }
  getStations() {
    this.spinner.show()
    this.api.do('get', 'admin/stations/list', {}, true).then(res => {
      this.spinner.hide()
      this.allStations = res['data']
      console.log(res);
    }, err => {
      console.log(err);
    })
  }
  clearForm() {
    this.model = {
      name: "",
      mail: "",
      phNumber: "",
      data: {
        district: "",
        city: "",
        latitute: "",
        longitude: ""
      }

    }
  }

}
