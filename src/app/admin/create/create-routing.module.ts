import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NguiDatetimePickerModule } from '@ngui/datetime-picker';
import { CommonModule } from '@angular/common';
import { CreateComponent } from './create.component';

const routes: Routes = [
    {path:'',component:CreateComponent}
]

@NgModule({
    imports: [RouterModule.forChild(routes),FormsModule,ReactiveFormsModule,NguiDatetimePickerModule],
    exports: [RouterModule,CommonModule],
    declarations: [],
})
export class CreateRoutingModule { }