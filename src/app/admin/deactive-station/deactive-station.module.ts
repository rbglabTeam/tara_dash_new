import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DeactiveStationComponent } from './deactive-station.component';
import { DeactiveStationRoutingModule } from './deactive-station-routing.module';
import { NgxSpinnerModule } from 'ngx-spinner';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [DeactiveStationComponent],
  imports: [
    CommonModule,
    FormsModule,
    DeactiveStationRoutingModule,
    NgxSpinnerModule
  ],

  schemas:[
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class DeactiveStationModule { }
