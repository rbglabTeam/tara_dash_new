import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../../services/api.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-deactive-station',
  templateUrl: './deactive-station.component.html',
  styleUrls: ['./deactive-station.component.scss']
})
export class DeactiveStationComponent implements OnInit {
  allStations = [];
  stationId = "";
  entries = [];
  myDate = new Date();
  deactiveStatus;
  constructor(
    private route: ActivatedRoute,
    public user:UserService,
     public api: ApiService) {
      this.user.setAccidents([])
      }

  ngOnInit() {
    this.getStations();
    window.scrollTo(0, 0);
  }
  getStations() {
    this.api.do('get', 'admin/stations/list', {}, true).then(res => {
      this.allStations = res['data']
      console.log(res);
    }, err => {
      console.log(err);
    })
  }

  /**
   * Deactivation Station!
   * @param event
   * @param index
   * @param object
   */
  clickme(event, index, object) {
    //  let value;
    //  const sts = event.target.value;
    //  if(object.active === true){
    //    value = false;
    //  }else{
    //    value = true;
    //  }
  // this.allStations[index] = false;
    debugger
    let packet = {
      station_id: object._id,
      active: object.active,
      duration_date_time: this.myDate
    }
  //  this.allStations.splice(index, 1);
    this.api.do('post', 'admin/station/deactivation/', packet, true).then(res => {
      if (res['code'] === 200) {
        console.log('deactivate reposnse', res['data']);

      }
      console.log(res);
    }, err => {
      console.log(err);
    });
  }



}
