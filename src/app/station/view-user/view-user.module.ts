import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ViewUserComponent } from './view-user.component';
import { ViewUserRoutingModule } from './view-user-routing.module';
import { NgxPaginationModule } from 'ngx-pagination';
import { NgxSpinnerModule } from 'ngx-spinner';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NguiDatetimePickerModule } from '@ngui/datetime-picker';


@NgModule({
  declarations: [ViewUserComponent],
  imports: [
    CommonModule,ViewUserRoutingModule,NguiDatetimePickerModule,
    NgxSpinnerModule,
    NgxPaginationModule,
    FormsModule,ReactiveFormsModule
  ],
  schemas:[
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class ViewUserModule { }
