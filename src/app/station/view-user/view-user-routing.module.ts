import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { ViewUserComponent } from './view-user.component';

const routes: Routes = [
    {path:'',component:ViewUserComponent}
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule,CommonModule],
    declarations: [],
})
export class ViewUserRoutingModule { }