import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { NgxSpinnerService } from 'ngx-spinner';
import swal from 'sweetalert';
import { UserService } from 'src/app/services/user.service';


@Component({
  selector: 'app-view-user',
  templateUrl: './view-user.component.html',
  styleUrls: ['./view-user.component.scss']
})
export class ViewUserComponent implements OnInit {
  allUsers = [];
  myDate = new Date();
  entries = [];
  model = {
    StartDate: "",
    EndDate: ""
  }
  error: any = { isError: false, errorMessage: '' };
  isValidDate: any;
  config: any;

  constructor(public api: ApiService,
    public user: UserService,
    private spinner: NgxSpinnerService) {
    this.entries = this.user.getAccidents();
    this.config = {
      itemsPerPage: 10,
      currentPage: 1,
    };

  }

  ngOnInit() {
    window.scrollTo(0, 0);
    // this.getUsers();
  }

  getUsers() {
    this.api.do('get', 'dash/station/user/list', {}, true).then(res => {
      this.allUsers = res['data'];
      console.log(res);
    }, err => {
      console.log(err);
    })
  }
  // paginationevent
  pageChanged(event) {
    this.config.currentPage = event;
  }
  clickme() {
    this.entries = []
    let packet = {
      from_date: this.model.StartDate,
      to_date: this.model.EndDate
    }
    let currentDate = new Date();
    this.isValidDate = this.validateDates(this.model.StartDate, this.model.EndDate, currentDate);
    if (this.isValidDate == true) {
      this.spinner.show();
      this.api.do('get', 'dash/station/accident/list', packet, true).then(res => {

        this.spinner.hide();
        swal({
          title: res['status'],
          icon: res['icon'],

          timer: 1000
        });
        if (res['code'] === 200) {
          let data = res['data'];
          data.forEach(element => {
            let modified = {
              date: element.duration_date_time.slice(0, 10),
              hourMinute: element.hourMinute,
              _id: element._id
            }
            this.entries.push(modified)
          });
          this.user.setAccidents(this.entries)

        }
        console.log(res);
      }, err => {
        console.log(err);
      });
    }
  }

  validateDates(sDate: string, eDate: string, currentDate: Object) {
    // this.isValidDate = true;
    if ((sDate == "" || eDate == "")) {
      this.error = {
        isError: true, errorMessage: swal({
          text: "Start date and end date are required",
          icon: "warning",
          dangerMode: true,
        })
      };
      this.isValidDate = false;
    }

    else if ((eDate) < (sDate)) {
      this.error = {
        isError: true, errorMessage: swal({
          text: "To date should be grater then From date.",
          icon: "warning",
          dangerMode: true,
        })
      };
      this.isValidDate = false;
    }
    else if ((currentDate) < (sDate) || (currentDate) < (eDate)) {
      this.error = {
        isError: true, errorMessage: swal({
          text: "Future date should not be selected",
          icon: "warning",
          dangerMode: true,
        })
      };
      this.isValidDate = false;
    }
    else {
      this.isValidDate = true
    }
    return this.isValidDate;
  }

}
