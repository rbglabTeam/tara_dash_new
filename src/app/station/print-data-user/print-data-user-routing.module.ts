import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { PrintDataUserComponent } from './print-data-user.component';

const routes: Routes = [
    {path:'',component:PrintDataUserComponent}
]

@NgModule({
    imports: [RouterModule.forChild(routes),FormsModule],
    exports: [RouterModule,CommonModule],
    declarations: [],
})
export class PrintDataUserRoutingModule { }