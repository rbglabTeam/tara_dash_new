import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { ApiService } from '../../services/api.service';
import * as jspdf from 'jspdf';
import { ExportAsService, ExportAsConfig } from 'ngx-export-as';
import { NgxSpinnerService } from 'ngx-spinner';
import { UserService } from 'src/app/services/user.service';
@Component({
  selector: 'app-print-data-user',
  templateUrl: './print-data-user.component.html',
  styleUrls: ['./print-data-user.component.scss']
})
export class PrintDataUserComponent implements OnInit {
  accidentData: [];
  _id = ''
  constructor(
    public route: ActivatedRoute,
     public api: ApiService,
    private spinner: NgxSpinnerService,
    public user:UserService,
     private exportAsService: ExportAsService) {
    this._id = this.route.snapshot.paramMap.get("id");
    if (this._id)
      this.getAccident();
    else {
      console.log("llll")
    }
  }

  ngOnInit() {
    window.scrollTo(0, 0);
  }
  get getUrl() {
    return this.api.serverUrl + 'media/image?image='
    // return "https://secret-savannah-48267.herokuapp.com/media/image?image="
  }
  get getAudio() {
    return this.api.serverUrl + 'media/audio?audio='
    // return "https://secret-savannah-48267.herokuapp.com/media/audio?audio="
  }
  get getVideo() {
    return this.api.serverUrl + 'media/video?video='
    // return "https://secret-savannah-48267.herokuapp.com/media/video?video="
  }
  // get getUrl() {
  //   return "https://secret-savannah-48267.herokuapp.com/media/image?image="
  // }
  // get getAudio() {
  //   return "https://secret-savannah-48267.herokuapp.com/media/audio?audio="
  // }
  // get getVideo() {
  //   return "https://secret-savannah-48267.herokuapp.com/media/video?video="
  // }
  getAccident() {
    console.log()
    let packet = {
      accident_id: this._id
    }

    this.api.do('get', 'dash/station/accident', packet, true).then(res => {

      if (res['code'] === 200) {
        this.accidentData = res["data"]["data"]
      }
      console.log(res);
    }, err => {
      console.log(err);
    })

  }

  printFunctionApi() {
    this.spinner.show();
    this.api.printFunction(this._id).then(res => {
      this.spinner.hide();
      const bearer = 'data:application/pdf;base64,'
      const pdfInBase64 = bearer + res;
      const linkSource = pdfInBase64;
      const downloadLink = document.createElement("a");
      const fileName = `${this._id}.pdf`;

      downloadLink.href = linkSource;
      downloadLink.download = fileName;

      downloadLink.click();

    })
  }

  public captureScreen() {
    var table = document.getElementById("div1");
    var currentHeight = 20
    var max = 300
    let pdf = new jspdf('p', 'mm', 'a4'); // A4 size page of PDF
    pdf.text(60, 10, 'Transportation Accident Report');
    pdf.setFontSize(12);
    pdf.setFontStyle('Arial, Helvetica, sans-serif');
    for (var i = 1; i < table['rows'].length; i++) {

      if ((currentHeight + 30) > max) {
        pdf.addPage()
        currentHeight = 20
      }
      var textvalue = table['rows'][i].cells[0].innerHTML + ' ----- ' + table['rows'][i].cells[1].innerHTML
      if (textvalue.includes('http') || textvalue.includes('"ng-reflect-ng-if')) {
        console.log('media');
      }
      else {
        pdf.text(20, currentHeight, textvalue);
        currentHeight = currentHeight + 10;
      }
    }
    pdf.save('TARA Case Report.pdf');
  }
}
