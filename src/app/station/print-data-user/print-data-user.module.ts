import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PrintDataUserComponent } from './print-data-user.component';
import { PrintDataUserRoutingModule } from './print-data-user-routing.module';
import { NgxSpinnerModule } from 'ngx-spinner';

@NgModule({
  declarations: [PrintDataUserComponent],
  imports: [
    CommonModule,
    PrintDataUserRoutingModule,
    NgxSpinnerModule
  ]
})
export class PrintDataUserModule { }
