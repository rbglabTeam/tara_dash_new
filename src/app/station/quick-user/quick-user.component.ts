import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-quick-user',
  templateUrl: './quick-user.component.html',
  styleUrls: ['./quick-user.component.scss']
})
export class QuickUserComponent implements OnInit {
  myDate = new Date();
  entries = [];
  constructor(public api: ApiService,
    public user:UserService,
    private spinner: NgxSpinnerService) {
      this.user.setAccidents([])
     }

  ngOnInit() {
    window.scrollTo(0, 0);
  }
  clickme() {
    this.entries = []
    this.spinner.show();
    let packet = {
      duration_date_time: this.myDate,
    }

    this.api.do('get', 'dash/station/accident/list', packet, true).then(res => {
      this.spinner.hide();
      if (res['code'] === 200) {
        this.entries = res['data'];

      }
      console.log(res);
    }, err => {
      console.log(err);
    });

  }

}
