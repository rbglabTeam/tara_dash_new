import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { UserService } from '../../services/user.service';
import { ApiService } from '../../services/api.service';
import swal from 'sweetalert';
import { NgxSpinnerService } from 'ngx-spinner';


@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss']
})
export class AddUserComponent implements OnInit {
  station: any = {};
  data = {};
  allUsers = [];

  constructor(
    private formBuilder: FormBuilder,
     private route: ActivatedRoute, 
     public spinner:NgxSpinnerService,
     public user: UserService,
    public api: ApiService) {
        this.user.setAccidents([])
    this.station = {
      name: "",
      mail: "",
      phNumber: "",
      token: this.api.token,
      data: {
        designation: ""
      }
    }
  }
  // nisha310198@gmail.com

  ngOnInit() {
    this.getUsers();
    window.scrollTo(0, 0);
  }
  onSubmit() {
    this.spinner.show()
    this.api.do('post', 'dash/station/user/create', this.station, true).then(res => {
      this.spinner.hide()
      if (res['password']) {
        swal({
          title: res['password'],
          text: "Please check registered email for password",
          icon: res['icon'],
        });

        console.log(res);
        this.clearForm();
      } else {
        swal({
          title: res['statusCode'],
          text: res['status'],
          icon: res['icon'],
        });
      }
    }, err => {
      console.log(err);
    })
  }
  getUsers() {
    this.spinner.show()
    this.api.do('get', 'dash/station/user/list', {}, true).then(res => {
      this.spinner.hide()
      this.allUsers = res['data'];
      console.log(res);
    }, err => {
      console.log(err);
    });
  }

  clearForm() {
    this.station = {
      name: "",
      mail: "",
      phNumber: "",
      data: {
        designation: ""

      }
    }
  }
}

