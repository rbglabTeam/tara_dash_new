import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DeactiveUserComponent } from './deactive-user.component';
import { DeactiveUserRoutingModule } from './deactive-user-routing.module';
import { NgxSpinnerModule } from 'ngx-spinner';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [DeactiveUserComponent],
  imports: [
    CommonModule,DeactiveUserRoutingModule,NgxSpinnerModule,FormsModule
  ],
  schemas:[
    CUSTOM_ELEMENTS_SCHEMA
  ],
})
export class DeactiveUserModule { }
