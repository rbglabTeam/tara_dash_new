import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DeactiveUserComponent } from './deactive-user.component';

const routes: Routes = [
    {path:'',component:DeactiveUserComponent}
]

@NgModule({
    imports: [RouterModule.forChild(routes),FormsModule,ReactiveFormsModule],
    exports: [RouterModule,CommonModule],
    declarations: [],
})
export class DeactiveUserRoutingModule { }