import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../../services/api.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-deactive-user',
  templateUrl: './deactive-user.component.html',
  styleUrls: ['./deactive-user.component.scss']
})
export class DeactiveUserComponent implements OnInit {
  allUsers = [];
  myDate = new Date();
  constructor(
    private route: ActivatedRoute,
    public user:UserService,
     public api: ApiService) {
      this.user.setAccidents([])
     }

  ngOnInit() {
    this.getUsers();
    window.scrollTo(0, 0);
  }
  getUsers() {
    this.api.do('get', 'dash/station/user/list', {}, true).then(res => {
      this.allUsers = res['data'];
      console.log(res);
    }, err => {
      console.log(err);
    });
  }
  /**
   * Deactivation Users!
   * @param event
   * @param index
   * @param object
   */
  clickme(event, index, object) {

    // let value;
    // const sts = event.target.value;
    // if(object.active === true){
    //   value = false;
    // }else{
    //   value = true;
    // }
 // this.allStations[index] = false;
   debugger
   let packet = {
     user_id: object._id,
     active: object.active,
     duration_date_time: this.myDate
   }
 //  this.allStations.splice(index, 1);
       this.api.do('post', 'dash/station/deactivation/users', packet, true).then(res => {
        if (res['code'] === 200) {
          console.log('deactivate reposnse', res['data']);

        }
        console.log(res);
      }, err => {
        console.log(err);
      });
    }


}
