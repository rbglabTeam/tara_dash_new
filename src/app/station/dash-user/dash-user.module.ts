import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { DashUserRoutingModule } from './dash-user-routing.module';
import { NguiDatetimePickerModule } from '@ngui/datetime-picker';
import { NgxSpinnerService, NgxSpinnerModule } from 'ngx-spinner';
import { DashUserComponent } from './dash-user.component';

@NgModule({
  declarations: [DashUserComponent],
  imports: [
    CommonModule,
    FormsModule,
    DashUserRoutingModule,
    NguiDatetimePickerModule,
    NgxSpinnerModule
  ],
  schemas:[
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class DashUserModule { }
