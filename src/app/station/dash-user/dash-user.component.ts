import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { NgxSpinnerService } from 'ngx-spinner';
import swal from 'sweetalert';
import { UserService } from 'src/app/services/user.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as _ from 'lodash';


@Component({
  selector: 'app-dash-user',
  templateUrl: './dash-user.component.html',
  styleUrls: ['./dash-user.component.scss']
})
export class DashUserComponent implements OnInit {
  dashUserForm: FormGroup;
  submitted = false;
  allUsers = [];
  myDate = new Date();
  entries = [];
  model = {
    StartDate: "",
    EndDate: ""
  }
  isValidDate: any;
  dashDetails: any;
   map = new Map();
   dateData = new Map();
  error: any = { isError: false, errorMessage: '' };


  constructor(
    public api: ApiService,
    public user: UserService,
    private formBuilder: FormBuilder,
    public spinner: NgxSpinnerService) {
    this.user.setAccidents([])
  }

  ngOnInit() {
    // this.getUsers();
    this.getDashdetails()
    window.scrollTo(0, 0);
  }


  getDashdetails(){

    this.spinner.show()

    this.api.do('get','dash/station/stations/details', {}, true).then(res=>{

    this.spinner.hide()

      this.dashDetails = res['data'];
      this.map = res['data']['station_name'];
      // console.log('response',this.map)
    },err=>{
      console.log(err);

    })
  }

  // getUsers() {
  //   this.spinner.show()
  //   this.api.do('get', 'dash/station/user/list', {}, true).then(res => {
  //     this.spinner.hide()
  //     this.allUsers = res['data'];
  //     console.log(res);
  //   }, err => {
  //     console.log(err);
  //   });
  // }
  // clickme() {
  //   const packet = {
  //     duration_date_time: this.myDate
  //   };
  //   this.entries = [];
  //   this.api.do('get', 'dash/station/accident/list/uniq', packet, true).then(res => {
  //     if (res['code'] === 200) {
  //       this.entries = res['data'];

  //     }
  //     console.log(res);
  //   }, err => {
  //     console.log(err);
  //   });

  // }


  
  /**
   *
   * @param dataobject update the get data from both two dates
   */
  updateDateDetails(dataobject){
    debugger;

    var value =  _.assign(this.map, _.toPlainObject(new this.Foo(this.map,dataobject)));
  }
  Foo(object,dataobject) {
   object.forEach(element => {
          // element.current_date_case_count=5
          dataobject.forEach(data => {
           // element.current_date_case_count=5
          if(element.station_mail === data.station_mail){
           element.current_date_case_count= data.current_date_case_count;
          }
       });
      });
  }
  /**
 * Get data between two dates!
 */
clickme() {

  let packet = {
    from_date: this.model.StartDate,
    to_date: this.model.EndDate
  }
  console.log("sucess");
  let currentDate = new Date();
  this.isValidDate = this.validateDates(this.model.StartDate, this.model.EndDate, currentDate);
  if (this.isValidDate === true){
    this.spinner.show()
    this.api.do('post', 'dash/station/stations/user/list', packet, true).then(res => {
      this.spinner.hide()
      if (res['code'] === 200) {
     this.dateData = res['data'];
         if(this.dateData){
           this.onCurrentDateRefresh();
           this.updateDateDetails( this.dateData);

         }

      } else {
        this.onCurrentDateRefresh();
        swal({
          title: res['status'],
          icon: res['icon'],
          timer: 1000
        });
      }
      console.log(res);
    }, err => {
      console.log(err);
    });
  }
}
  /**
   * reset current date case count property value!
   */


  onCurrentDateRefresh(){
    this.dashDetails.station_name.forEach(element => {
       element.current_date_case_count = 0;
    });
   }

   validateDates(sDate: string, eDate: string, currentDate: Object) {
     // this.isValidDate = true;
     if ((sDate == "" || eDate == "")) {
       this.error = {
         isError: true, errorMessage: swal({
           text: "Start date and end date are required",
           icon: "warning",
           dangerMode: true,
         })
       };
       this.isValidDate = false;
     }

     else if ((eDate) < (sDate)) {
       this.error = {
         isError: true, errorMessage: swal({
           text: "To date should be grater then From date.",
           icon: "warning",
           dangerMode: true,
         })
       };
       this.isValidDate = false;
     }
     else if ((currentDate) < (sDate) || (currentDate) < (eDate)) {
       this.error = {
         isError: true, errorMessage: swal({
           text: "Future date should not be selected",
           icon: "warning",
           dangerMode: true,
         })
       };
       this.isValidDate = false;
     }
     else {
       this.isValidDate = true
     }
     return this.isValidDate;
   }

}
