import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashUserComponent } from './dash-user.component';
import { NguiDatetimePickerModule } from '@ngui/datetime-picker';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';

const routes: Routes = [
    {path:'dashUser',component:DashUserComponent}
]

@NgModule({
    imports: [RouterModule.forChild(routes),FormsModule],
    exports: [RouterModule,CommonModule],
    declarations: [],
})
export class DashUserRoutingModule { }