import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SendMailComponent } from './send-mail.component';
import { CommonModule } from '@angular/common';

const routes: Routes = [
    {path:'',component:SendMailComponent}
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule,CommonModule],
    declarations: [],
})
export class SendMailRoutingModule { }