import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SendMailRoutingModule } from './send-mail-routing.module';
import { FormsModule } from '@angular/forms';
import { SendMailComponent } from './send-mail.component';

@NgModule({
  declarations: [SendMailComponent],
  imports: [
    CommonModule,
    SendMailRoutingModule,
    FormsModule
  ],
  schemas:[
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class SendMailModule { }
