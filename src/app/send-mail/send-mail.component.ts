import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { UserService } from '../services/user.service';
import { ApiService } from '../services/api.service';
import swal from 'sweetalert';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-send-mail',
  templateUrl: './send-mail.component.html',
  styleUrls: ['./send-mail.component.scss']
})
export class SendMailComponent implements OnInit {


  model: any = {};
  data = {}
  allStations = [];
  selectedMails=[]
  filterData: {};
  local: any;

  constructor(private formBuilder: FormBuilder, private route: ActivatedRoute, public spinner: NgxSpinnerService,
    public user: UserService, public api: ApiService) {
    this.model = {
      fromMail: "",
      toMail: this.selectedMails,
      token: this.api.token,
      mailSubject:""
    }
  }

  ngOnInit() {
    this.getStations();
    this.getLocalstorage()
    window.scrollTo(0, 0);
  }
  getMails(v){
    this.selectedMails.push(v)
    console.log(this.selectedMails);
    
    
  }

  onSubmit() {

    this.spinner.show()
    console.log(this.model);
    
    // this.api.do('post', 'admin/station/create', this.model, true).then(res => {
    //   this.spinner.hide()
    //   if (res['password']) {
    //     swal({
    //       title: res['password'],
    //       text: "Please check registered email for password",
    //       icon: res['icon'],
    //     });
    //     console.log(res);
    //     this.clearForm();
    //   } else {
    //     swal({
    //       title: res['statusCode'],
    //       text: res['status'],
    //       icon: res['icon'],
    //     });
    //   }
    // }, err => {
    //   console.log(err);
    // });

  }
  getStations() {
    this.spinner.show()
    this.api.do('get', 'admin/stations/list', {}, true).then(res => {
      this.spinner.hide()
      this.allStations = res['data']
      console.log(res);
    }, err => {
      console.log(err);
    })
  }
  clearForm() {
    this.model = {
      toMail: "",
      toMails: "",
      mailSubject:""
    }
  }

  getLocalstorage(): any {

    this.local = JSON.parse(localStorage.getItem('currentUserDash'))
    this.model.fromMail=this.local.mail
}

  districts = [
    'Kanchipuram',
    'Tiruvallur',
    'Cuddalore',
    'Villupuram',
    'Vellore',
    'Tiruvannamalai',
    'Salem',
    'Namakkal',
    'Dharmapuri',
    'Erode',
    'Coimbatore',
    'The Nilgiris',
    'Thanjavur',
    'Nagapattinam',
    'Tiruvarur',
    'Tiruchirappalli',
    'Karur',
    'Perambalur',
    'Pudukkottai',
    'Madurai',
    'Theni',
    'Dindigul',
    'Ramanathapuram',
    'Virudhunagar',
    'Sivagangai',
    'Tirunelveli',
    'Thoothukkudi',
    'Kanniyakumari',
    'Krishnagiri',
    'Ariyalur',
    'Tiruppur',
    'Chennai'
  ];

}
